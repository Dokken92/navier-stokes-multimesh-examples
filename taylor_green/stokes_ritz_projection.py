from dolfin import *
from functools import reduce
import numpy as np
from dolfin.fem.assembling import assemble_multimesh

def StokesRitz(nu, t0, T, dt, multimesh, degrees=(2,1), 
               alpha=10, beta=10, alphaR=10, betaR=10, delta=0.05,
               results_dir="results_multimesh"):

    (degree_u, degree_p), degree_rise = degrees, 3
    
    mesh_0 = multimesh.part(0)
    # build function spaces for velocity
    V_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u)
    V = MultiMeshFunctionSpace(multimesh, V_el)
    u_h = MultiMeshFunction(V)
    # DEBUG
    u_ex_h = MultiMeshFunction(V)
    
    # build function space for pressure
    Q_el = FiniteElement("CG", mesh_0.ufl_cell(), degree_p)
    R_el = FiniteElement("R", mesh_0.ufl_cell(), 0)
    
    Q = MultiMeshFunctionSpace(multimesh, Q_el)
    p_h = MultiMeshFunction(Q)
    p_c = MultiMeshFunction(Q)
    p_ex_h = MultiMeshFunction(Q)

    # Analytical solution
    x = SpatialCoordinate(multimesh)
#     u_ex = as_vector((x[1]*x[1], x[0]*x[0]))
#     p_ex = 2*x[0] + 2*x[1]
#     f_ex = Constant((0,0))
    
    t = Constant(0)
    u_ex = as_vector((-(cos(pi*(x[0]))*sin(pi*(x[1])))*exp(-2.0*nu*pi*pi*t),
                (cos(pi*(x[1]))*sin(pi*(x[0])))*exp(-2.0*nu*pi*pi*t)))
    p_ex = -0.25*(cos(2*pi*(x[0])) + cos(2*pi*(x[1])))*exp(-4.0*nu*pi*pi*t)
    
    f_ex = 2*pi**2* u_ex 
    grad_p = 0.5*pi*as_vector((sin(2*pi*x[0]), sin(2*pi*x[1])))
    f_ex += grad_p
 
    # set-up for u error computation
    degree_rise = 3
    V_err_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u+degree_rise)
    V_err = MultiMeshFunctionSpace(multimesh, V_err_el)
    u_err = MultiMeshFunction(V_err)
    u_err.interpolate(Constant((0,0)))
    h1_errors_u = []
    l2_errors_u = []
    l2_errors_p = []
    
    # set-up for p error computation
    Q_err_el = FiniteElement("CG", mesh_0.ufl_cell(), degree=degree_p+degree_rise)
    Q_err = MultiMeshFunctionSpace(multimesh, Q_err_el)
    p_err = MultiMeshFunction(Q_err)
    p_err.interpolate(Constant(0))

    n = FacetNormal(multimesh)
    h = 2.0*Circumradius(multimesh)
    def tensor_jump(v, n):
        return outer(v('+'), n('+')) + outer(v('-'), n('-'))
    
    # Step 0: Compute Stokes projection of the initial velocity
    # Viscosity terms
    VQR = MultiMeshFunctionSpace(multimesh, MixedElement([V_el, Q_el, R_el]))
    u,p,r = TrialFunctions(VQR)
    v,q,s = TestFunctions(VQR)
    
    def rj(v, n=None):
        if n:
            return inner(v("+"),n("+"))+ inner(v("-"),n("-"))
        else: 
            return  v("+") - v("-")
        
    def a_h(u, v):
        a = nu*(inner(grad(u), grad(v))*dX +  
                - inner(avg(grad(u)), tensor_jump(v, n))*dI 
                - inner(avg(grad(v)), tensor_jump(u, n))*dI
                + alpha/avg(h)*inner(rj(u), rj(v))*dI)
                 
        # Add Nitsche terms on outer boundary
        a += nu*(- inner(grad(u)*n, v)*ds
              - inner(grad(v)*n, u)*ds
              + alpha/h*inner(u, v)*ds)
        return a
    
    def b_h(p, v):
        return -div(v)*p*dX + (inner(v("+"),n("+"))+ inner(v("-"),n("-")))*avg(p)*dI + inner(v, n)*p*ds 
    
    mu = 1
    tau = -1
    def s_h(u,p,v,q):
#         s =  -delta*h*h*inner(-div(grad(u)) + grad(p), -mu*div(grad(v)) + tau*grad(q))*dC
#         s += -delta*h("+")*h("+")*inner(-div(grad(u("+"))) + grad(p("+")), -mu*div(grad(v("+"))) + tau*grad(q("+")))*dO
#         s = -h*h*beta*inner(jump(grad(p)), jump(grad(q)))*dO

        # Symmetric pressure stabilizations
        s = -delta*avg(h)*avg(h)*inner(rj(grad(p)), rj(grad(q)))*dO
#         s = -inner(jump(p), jump(q))*dO
        s += beta*nu*inner(jump(grad(u)), jump(grad(v)))*dO
        return s
    
    def a_lm(p,r,q,s):
        a = (p*s + q*r)*dX
        a += alphaR/avg(h)**(degree_p+1)*inner(jump(r), jump(s))*dI
        a += betaR/avg(h)**(degree_p+2)*inner(jump(r), jump(s))*dO
        return a
    
    a0 = a_h(u,v) + b_h(p, v) + b_h(q, u)
    a0 += s_h(u,p,v,q)
    a0 += a_lm(p,r,q,s)
    
    # Compute rhs for Stokes-Ritz projector
    L0 = a_h(u_ex,v) + b_h(p_ex, v) + b_h(q, u_ex)
    
    A0 = assemble_multimesh(a0)
    b0 = assemble_multimesh(L0)
    
    # Create subspaces for boundary conditions
    Vsub = MultiMeshSubSpace(VQR, 0)

    VQR.lock_inactive_dofs(A0, b0)
    
    # Solve for projected velocity
    upr = MultiMeshFunction(VQR)
    solver_0 = LUSolver(A0, "mumps")
    solver_0.solve(upr.vector(), b0)

    # Compute contribution to interface terms in discrete divergence
    # Should be very close to 0 if u_h is in V_h and smooth
#     L2 = inner(jump(u_h, n), avg(q))*dI
#     b2 = assemble_multimesh(L2)
#     print(b2.str(True))
#     print("Norm of assembled  inner(jump(u_h, n), avg(q))*dI = %g" % b2.norm("l2"))
#     dI_value =   abs(assemble_multimesh(inner(jump(u_h), jump(u_h))*dI))
    
    for i in range(multimesh.num_parts()):
        u_h.assign_part(i, upr.part(i).sub(0))
        p_h.assign_part(i, upr.part(i).sub(1))
    
    for i in range(multimesh.num_parts()):
        xdmffile_u0 = XDMFFile(results_dir + "u0_part_%i.xdmf" % i)
        xdmffile_u0.write(u_h.part(i), 0)
        xdmffile_p0 = XDMFFile(results_dir + "p0_part_%i.xdmf" % i)
        xdmffile_p0.write(p_h.part(i), 0)
        
    return (u_h, p_h, u_ex, p_ex)
    

if __name__ == "__main__":
    nu = 1.0
    t0, T = 0, 0.5
    degrees = (2,1)
#     alpha, beta, alphaR, betaR, delta = 5, 1, 100, 100, 0.05
    alpha, beta, alphaR, betaR, delta = 50, 10, 100, 100, 0.05
    num_ref = 0
    dt_0 = 0.1
    dt = 0.1
    N_0 = 10
    
    run_num = 1
    results_top_dir = "results_run_%d_multimesh_num_meshes_%i/" % (run_num, 2)
    h1_errors_u = []
    l2_errors_u = []
    l2_errors_p = []
    dI_values = []
    
    for N in [N_0*2**k for k in range(num_ref+1)]:
            # Build MultiMeshes
            mesh_0 = RectangleMesh(Point(-1,-1), Point(1,1), N, N)
            mesh_1 = RectangleMesh(Point(-pi/6,-pi/6), Point(pi/6,pi/6), N, N)
            mesh_1.rotate(pi*10)
            multimesh = MultiMesh()
            for mesh in [mesh_0, mesh_1]:
                multimesh.add(mesh)
            multimesh.build(2*degrees[0])
            
            results_dir = results_top_dir + "/N_{}_dt_{}/".format(N,dt)
            u_h, p_h, u_ex, p_ex = StokesRitz(nu, t0, T, dt, multimesh, degrees, 
                    alpha=alpha, beta=beta, alphaR=alphaR, betaR=betaR, delta=delta,
                    results_dir=results_dir)
            
            h1_error_u = abs(assemble_multimesh((grad(u_ex-u_h))**2*dX))**0.5
            l2_error_u = abs(assemble_multimesh((u_ex-u_h)**2*dX))**0.5
            l2_error_p = abs(assemble_multimesh((p_ex-p_h)**2*dX))**0.5
            dI_value =   abs(assemble_multimesh(inner(jump(u_h), jump(u_h))*dI))

            print("========================================")
            print("Computed errors for Stokes projection:")
            print("----------------------------------------")
            print("H1 error for u_0: %g" % h1_error_u)
            print("L2 error for u_0: %g" % l2_error_u)
            print("L2 error for p_0: %g" % l2_error_p)
            print("|| [u] ||^2_I =  %g" % dI_value)
    
            h1_errors_u.append(h1_error_u)
            l2_errors_u.append(l2_error_u)
            l2_errors_p.append(l2_error_p)
            dI_values.append(dI_value)
    
    h1_errors_u = np.array(h1_errors_u)
    l2_errors_u = np.array(l2_errors_u)
    l2_errors_p = np.array(l2_errors_p)
    
    eoc_h1_u = np.log(h1_errors_u[:-1]/h1_errors_u[1:])/np.log(2)
    eoc_l2_u = np.log(l2_errors_u[:-1]/l2_errors_u[1:])/np.log(2)
    eoc_l2_p = np.log(l2_errors_p[:-1]/l2_errors_p[1:])/np.log(2)
    
    print(eoc_h1_u)
    print(eoc_l2_u)
    print(eoc_l2_p)

