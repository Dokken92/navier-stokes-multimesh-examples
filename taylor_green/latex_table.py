import numpy as np
from tabulate import tabulate
from IPython import embed

def latex_float(f):
    float_str = "{0:.2e}".format(f)
    base, exponent = float_str.split("e")
    return "{0} \\cdot 10^{{{1}}}".format(base, int(exponent))

def simple_table(errors):    
    num_ref_time, num_ref_space = errors.shape
    num_ref_time -=1
    num_ref_space -=1

    d = [[None for i in range(num_ref_space+1+2)] for j in range(num_ref_time+1+3)]
    d[0][0] = "Lt /Lx"
    d[0][1:] = range(0, num_ref_space+2)
    for i in range(1, num_ref_time+2):
        d[i][0] = "%d" %(i-1)
    d[0][-1] = "eoct"
    d[-2][0] = "eocx"
    d[-1][0] = "eocxt"
    out_np = np.array(d)
    M = errors.shape[0] 
    N = errors.shape[1] 
    str_err = [[None for i in range(M)] for j in range(N)] 
    for i in range(N): 
        for j in range(M): 
            str_err[i][j] = "{0:.2e}".format(errors[i,j])
    out_np[1:-2, 1:-1] = str_err
    # Add spatial convergence
    eoc_s = ["{0:.2f}".format(e) for e in eoc(errors[-1,:])]
    out_np[-2,2:-1] = eoc_s
    # Add temporal convergence
    eoc_t = ["{0:.2f}".format(e) for e in eoc(errors[:,-1])]
    out_np[2:-2,-1] = eoc_t
    end_st = min(num_ref_time, num_ref_space)
    eoc_st = ["{0:.2f}".format(e)
              for e in eoc(np.array(
                      [errors[i,i] for i in
                       range(end_st+1)]))]
    out_np[-1,2:end_st+2] = eoc_st
    return tabulate(out_np, tablefmt="simple")


def latex_table(errors):
    
    num_ref_time, num_ref_space = errors.shape
    num_ref_time -=1
    num_ref_space -=1

    d = [[None for i in range(num_ref_space+1+2)] for j in range(num_ref_time+1+3)]
    d[0][0] = r"$L_t\downarrow\backslash L_x\rightarrow$"
    d[0][1:] = range(0, num_ref_space+2)
    for i in range(1, num_ref_time+2):
        extra = "\hline" if i==1 else ""
        d[i][0] = extra+"%d" %(i-1)
    d[0][-1] = "$eoc_t$"
    d[-2][0] = "\hline$eoc_x$"
    d[-1][0] = "\hline$eoc_{xt}$"
    out_np = np.array(d)
    M = errors.shape[0] 
    N = errors.shape[1] 
    str_err = [[None for i in range(M)] for j in range(N)] 
    for i in range(N): 
        for j in range(M): 
            str_err[i][j] = "${0:s}$".format(latex_float(errors[i,j]))
    out_np[1:-2, 1:-1] = str_err
    # Add formatting due to visibility
    for i in range(1,len(out_np[1:-2])):
        out_np[i,-2] = "$\\mathit{{" + out_np[i,-2].strip("$") + "}}$"
    for i in range(1,len(out_np[1:-2])):
        out_np[-3, i] = "$\\mathbf{{" + out_np[-3,i].strip("$") + "}}$"
    for i in range(1,len(out_np[1:-2])):
        out_np[i, i] = "$\\underline{{" + out_np[i,i].strip("$") + "}}$"
    out_np[-3,-2] = "$\\underline{{\\mathbfit{{" + out_np[-3,-2].strip("$") + "}} }}$"
    # Add spatial convergence
    eoc_s = ["${0:.2f}$" .format(e) for e in eoc(errors[-1,:])]
    # Extra formatting
    for i in range(len(eoc_s)):
        eoc_s[i] = "$\\mathbf{{" + eoc_s[i].strip("$") + "}}$" 
    out_np[-2,1] = "$-$"
    out_np[-2,2:-1] = eoc_s
    # Add temporal convergence
    eoc_t = ["${0:.2f}$".format(e) for e in eoc(errors[:,-1])]
    # Extra formatting
    for i in range(len(eoc_t)):
        eoc_t[i] = "$\\mathit{{" + eoc_t[i].strip("$") + "}}$" 
    out_np[1,-1] = "$-$"
    out_np[2:-2,-1] = eoc_t
    end_st = min(num_ref_time, num_ref_space)
    eoc_st = ["${0:.2f}$".format(e)
              for e in eoc(np.array(
                      [errors[i,i] for i in
                       range(end_st+1)]))]
    for i in range(len(eoc_st)):
        eoc_st[i] = "$\\underline{{" + eoc_st[i].strip("$") + "}}$" 
    out_np[-1,1] = "$-$"
    out_np[-1,2:end_st+2] = eoc_st
    return tabulate(out_np, tablefmt="latex_raw")

def eoc(error_list):
    np_err = np.array(error_list)
    return np.log(error_list[:-1]/error_list[1:])/np.log(2)

def spatial_table(BDF2=True, implicit=False):
    if BDF2:
        extra_str = "BDF2"
    else:
        extra_str = "CN"
    if implicit:
        extra_str += "_implicit"
    else:
        extra_str += "_explicit"
    filename = "results_spatial_{0:s}/error_dict.npy".format(extra_str)
    indict = np.load(filename)
    for key in indict.item().keys():
        print(key)
        print(latex_table(indict.item().get(key)), file=open("texfiles/"+key+"{0:s}.tex".format(extra_str), "w"))

def temporal_table(BDF2=True, implicit=False):
    if BDF2:
        extra_str = "BDF2"
    else:
        extra_str = "CN"
    if implicit:
        extra_str += "_implicit"
    else:
        extra_str += "_explicit"
    filename = "results_temporal_{0:s}/error_dict.npy".format(extra_str)

    indict = np.load(filename)
    for key in indict.item().keys():
        print(latex_table(indict.item().get(key)), file=open("texfiles/"+key+"_P4P3{0:s}.tex".format(extra_str), "w"))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    feature_parser = parser.add_mutually_exclusive_group(required=True)
    feature_parser.add_argument('--spatial', dest='spatial',
                                action='store_true')
    feature_parser.add_argument('--temporal', dest='spatial',
                                action='store_false')
    feature_parser2 = parser.add_mutually_exclusive_group(required=False)
    feature_parser2.add_argument('--BDF2', dest='BDF2',
                                 action='store_true')
    feature_parser2.add_argument('--CN', dest='BDF2',
                                 action='store_false')
    feature_parser3 = parser.add_mutually_exclusive_group(required=False)
    feature_parser3.add_argument('--implicit', dest='implicit',
                                action='store_true')
    feature_parser3.add_argument('--explicit', dest='implicit',
                                 action='store_false')
    parser.set_defaults(BDF2=True, implicit=False)
    args = parser.parse_args()
    import sys
    thismodule = sys.modules[__name__]
    for key in vars(args):
        setattr(thismodule, key, getattr(args, key))

    if spatial:
        spatial_table(BDF2=BDF2, implicit=implicit)
    else:
        temporal_table(BDF2=BDF2, implicit=implicit)
