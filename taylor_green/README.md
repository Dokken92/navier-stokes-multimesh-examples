Usage
------
To reproduce results for spatial convergence:

`python3 MM_IPCS_Taylor_Green_BDF2.py --spatial`

and update tables in paper after simulation is finished:

`python3 latex_table.py --spatial`

Similarly for temporal convergence:

`python3 MM_IPCS_Taylor_Green_BDF2.py --temporal` and
`python3 latex_table.py --temporal`


Tables
-------

Tables for spatial and temporal convergence can be found in the `tex`-files.

Figures
-------
**An illustration of the coarsest MultiMesh used in the convergence study**

![A illustration of the coarsest MultiMesh used in the convergence study](multimesh.png)
