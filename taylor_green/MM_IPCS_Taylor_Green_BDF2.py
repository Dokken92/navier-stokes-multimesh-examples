import concurrent.futures
from tabulate import tabulate
from dolfin import *
import numpy as np
from stokes_ritz_projection import *

set_log_level(LogLevel.ERROR)
u_ex_str = ('-(cos(pi*(x[0]))*sin(pi*(x[1])))*exp(-2.0*nu*pi*pi*t)',
            ' (cos(pi*(x[1]))*sin(pi*(x[0])))*exp(-2.0*nu*pi*pi*t)')

p_ex_str = '-0.25*(cos(2*pi*(x[0])) + cos(2*pi*(x[1])))*exp(-4.0*nu*pi*pi*t)'
grad_p_ex_str = (
                 '0.5*pi*sin(2*pi*(x[0]))*exp(-4.0*nu*pi*pi*t)',
                 '0.5*pi*sin(2*pi*(x[1]))*exp(-4.0*nu*pi*pi*t)'
                 )
grad_u_ex_str = (('pi*(sin(pi*(x[0]))*sin(pi*(x[1])))*exp(-2.0*nu*pi*pi*t_)',
                  '-pi*(cos(pi*(x[0]))*cos(pi*(x[1])))*exp(-2.0*nu*pi*pi*t_)'),
                 ('pi*(cos(pi*(x[1]))*cos(pi*(x[0])))*exp(-2.0*nu*pi*pi*t_)',
                  '-pi*(sin(pi*(x[1]))*sin(pi*(x[0])))*exp(-2.0*nu*pi*pi*t_)'))
f = Constant((0,0))


class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

def get_meshes(N):
    # Build MultiMeshes
    mesh_0 = RectangleMesh(Point(-1,-1), Point(1,1), N, N, "crossed")
    mesh_1 = RectangleMesh(Point(-pi/6.11,-pi/6.22), Point(pi/6.33,pi/6.44),
                           N, N)
    mesh_2 = RectangleMesh(Point(0.03,0.01), Point(0.7,0.8),
                           int(0.5*N), int(0.5*N))
    mesh_2.rotate(-35)
    mesh_1.rotate(pi*10)

    return mesh_0, mesh_1, mesh_2


def IPCSSolver(nu, t0, T, dt, N, degrees, alpha,
               beta, delta, use_ritz, results_dir, implicit=False):
    (degree_u, degree_p), degree_rise = degrees, 3

    mesh_0, mesh_1, mesh_2 = get_meshes(N)
    multimesh = MultiMesh()
    for mesh in [mesh_0, mesh_1, mesh_2]:
        multimesh.add(mesh)
    multimesh.build(2*degree_u)
    num_parts = multimesh.num_parts()
        
    # build function spaces 
    V_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u)
    V = MultiMeshFunctionSpace(multimesh, V_el)

    Q_el = FiniteElement("CG", mesh_0.ufl_cell(), degree_p)
    Q = MultiMeshFunctionSpace(multimesh, Q_el)

    # Set-up functions
    u_h = MultiMeshFunction(V)
    u_old = MultiMeshFunction(V)
    u_s = MultiMeshFunction(V)

    p_h = MultiMeshFunction(Q)
    p_c = MultiMeshFunction(Q)

    # Initial values
    # TODO: replace by Stokes-Ritz projection (SRP)
    u_ex = Expression(u_ex_str, t=t0, nu=nu, degree=degree_u+degree_rise)

    # For BDF 2
    u_ex.t = t0-dt
    u_old.interpolate(u_ex)

    u_ex.t = t0
    u_h.interpolate(u_ex)

    p_ex = Expression(p_ex_str, t=t0, nu=nu, degree=degree_p+degree_rise)
    p_h.interpolate(p_ex)

    if use_ritz:
        u_h, p_h, u_ex0, p_ex0 = StokesRitz(nu, t0, T, dt, multimesh, degrees, 
                                            alpha=alpha, beta=beta,
                                            delta=delta,
                                          results_dir=results_dir)
    dt_c = Constant(dt)

    # Define forms 
    
    # Setup for step 1: computing tentative velocity
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # N.B. Full explicit, 1st order convection add_terms
    n = FacetNormal(multimesh)
    h = 2.0*Circumradius(multimesh)

    tensor_jump = lambda v, n: outer(v('+'), n('+')) + outer(v('-'), n('-'))

    a1 = (3/(2*dt_c)*inner(u, v) + nu*inner(grad(u),grad(v)))*dX
    # Implicit convection term
    if implicit:
        a1 += inner(grad(u)*(2*u_h - u_old),v)*dX + 0.5*inner(div(2*u_h - u_old)*u, v)*dX
    # Nitsche terms
    a1 += nu*(-inner(avg(grad(u)), tensor_jump(v, n))*dI
              -inner(avg(grad(v)), tensor_jump(u, n))*dI
              +alpha/avg(h)*inner(jump(u), jump(v))*dI)
    # Overlap stabilization for mass term
    # This might need scaling with k^2, where k is the polynomial degree of u
    a1+= beta*3/(2*dt_c)*inner(jump(u), jump(v))*dO
    # Overlap stabilization for viscosity term
    a1 += beta*nu*inner(jump(grad(u)), jump(grad(v)))*dO
    
    # N.B: For now, use exact solution for convection terms 
    # to mimic solving the time-dependent Stokes problem with
    # given exact solution (avoids any stability issure related 
    # to  convection terms/hyperbolic CFL condition)
    L1 = (1/(2*dt_c)*inner(4*u_h - u_old, v)
        + inner(p_h, div(v))
        + inner(f, v))*dX 
    if not implicit:
        L1 += (- 2*inner(grad(u_h)*u_h, v)
               + inner(grad(u_old)*u_old, v))*dX

    L1 -= avg(p_h)*inner(n("-"), v("-")-v("+"))*dI
    L1 -= jump(p_h)*inner(n("-"), avg(v))*dI

    # boundary conditions
    bc_u = MultiMeshDirichletBC(V, u_ex, DirichletBoundary())
    if not implicit:
        A1 = assemble_multimesh(a1)
    
    # Setup for step 2: compute pressure update
    p = TrialFunction(Q)
    q = TestFunction(Q)
    
    a2 = inner(grad(p), grad(q))*dX

    # Nitsche terms 
    a2 += (-dot(avg(grad(p)), jump(q, n))
           -dot(avg(grad(q)), jump(p, n))
           +alpha/avg(h)*inner(jump(p), jump(q)))*dI
    # Overlap stabilization for p
    a2 += beta*inner(jump(grad(p)), jump(grad(q)))*dO
    L2 = -3/(2*dt_c)*inner(div(u_s), q)*dX
    
    A2 = assemble_multimesh(a2)
    b2 = assemble_multimesh(L2)

    # Define nullspace
    null_vec = Vector(assemble_multimesh(L2))
    null_vec[:] = 1.0
    for part in range(Q.num_parts()):
        dofs = Q.dofmap().inactive_dofs(multimesh, part)
        null_vec[dofs] = 0.0
    null_vec *= 1.0/null_vec.norm("l2")
    nullspace_basis = [null_vec]
    nullspace = VectorSpaceBasis(nullspace_basis)
    as_backend_type(A2).set_nullspace(nullspace)

    # create solver and factorize matrix
    solver_2 = KrylovSolver('cg', 'amg')
    prm = solver_2.parameters
    prm["absolute_tolerance"] = 1e-12
    prm["relative_tolerance"] = 1e-12

    # Setup for step 3: update velocity
    a3 = inner(u, v)*dX
    # Overlap stabilization 
    a3 += beta*inner(jump(u), jump(v))*dO
    L3 = (inner(u_s, v) - 2*dt_c/3*inner(grad(p_c), v))*dX

    A3 = assemble_multimesh(a3)
    b3 = assemble_multimesh(L3)
    V.lock_inactive_dofs(A3, b3)
    
    # create solver and factorize matrix
    solver_3 = LUSolver(A3, "mumps")

    # time-stepping
    t = t0

    V_err_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u+degree_rise)
    V_err = MultiMeshFunctionSpace(multimesh, V_err_el)
    Q_err_el = FiniteElement("CG", mesh_0.ufl_cell(), degree=degree_p+degree_rise)
    Q_err = MultiMeshFunctionSpace(multimesh, Q_err_el)

    # Collect errors
    h1_errors_u = []
    l2_errors_u = []
    l2_errors_p = []
    
    # Set-up initial velocity
    # this is not need for actual computation
    # but xdmf writer seems to get confused 
    # if different objects are written to same file
    # (as in: attaches different names to them)
    u_ex_inter = MultiMeshFunction(V_err)
    u_ex_inter.interpolate(u_ex)
    
    p_ex_inter = MultiMeshFunction(Q_err)
    p_ex_inter.interpolate(p_ex)

    # Set-up output files
    
    # Define output files and write out data for initial step
    xdmffile_tent = [XDMFFile(results_dir + "u_tent_part_{0:d}.xdmf".format(i))
                     for i in range(num_parts)]
    xdmffile_u = [XDMFFile(results_dir + "u_part_{0:d}.xdmf".format(i))
                  for i in range(num_parts)]
    xdmffile_p = [XDMFFile(results_dir + "p_part_{0:d}.xdmf".format(i))
                  for i in range(num_parts)]

    for i in range(num_parts):
        xdmffile_tent[i].write(u_h.part(i), t)
        xdmffile_u[i].write(u_h.part(i), t)
        xdmffile_p[i].write(p_h.part(i), t)

    # Same for exact solutions
    xdmffile_u_ex = [XDMFFile(results_dir + "u_ex_part_{0:d}.xdmf".format(i))
                     for i in range(num_parts)]
    xdmffile_p_ex = [XDMFFile(results_dir + "p_ex_part_{0:d}.xdmf".format(i))
                     for i in range(num_parts)]
    for i in range(num_parts):
        xdmffile_u_ex[i].write(u_ex_inter.part(i), t)
        xdmffile_p_ex[i].write(p_ex_inter.part(i), t)

    # Solve problem
    sample_time = [i*T/10 for i in range(1,11)]
    while(t <= T-dt*10e-3):
        print(".", end="")
        # Increase time and update data
        t += dt
        u_ex.t = t
        p_ex.t = t
        # u_ex_h.interpolate(u_ex)
        # p_ex_h.interpolate(p_ex)
        # f.t = t
        #print("Time t = %e" % t)

        # Step 1: compute tentative velocity 
        b1 = assemble_multimesh(L1)
        if implicit:
            A1 = assemble_multimesh(a1)
        bc_u.apply(A1, b1)
        V.lock_inactive_dofs(A1, b1)
        solver_1 = LUSolver(A1, "mumps")
        solver_1.solve(u_s.vector(), b1)
        # Write out tentative velocity
        if any(np.isclose(t, ss) for ss in sample_time):
            for i in range(num_parts):
                xdmffile_tent[i].write(u_s.part(i), t)
        
        # Step 2: compute pressure correction
        b2 = assemble_multimesh(L2)
        Q.lock_inactive_dofs(A2, b2)
        nullspace.orthogonalize(b2)
        solver_2.solve(A2, p_c.vector(), b2)
        # Normalize pressure correction (avg value 0)
        vol = assemble_multimesh(Constant(1)*dx(domain=multimesh)
                                 +Constant(1)*dC(domain=multimesh))
        p_c.vector()[:] -= assemble_multimesh(p_c*dX)/vol
        # Update pressure
        p_h.vector().axpy(1.0, p_c.vector())
        # DEBUG
        # p_h.assign(p_c)
        # Write out computed p
        if any(np.isclose(t, ss) for ss in sample_time):
            for i in range(num_parts):
                xdmffile_p[i].write(p_h.part(i), t)
        
        # Assign new u_old
        u_old.assign(u_h)

        # Step 3: compute velocity update
        b3 = assemble_multimesh(L3)
        V.lock_inactive_dofs(A3, b3)
        solver_3.solve(u_h.vector(), b3)
        # Write out computed u
        if any(np.isclose(t, ss) for ss in sample_time):
            for i in range(num_parts):
                xdmffile_u[i].write(u_h.part(i), t)
        
        # Compute exact solution and write it out
        u_ex_inter.interpolate(u_ex)
        p_ex_inter.interpolate(p_ex)
        if any(np.isclose(t, ss) for ss in sample_time):
            for i in range(num_parts):
                xdmffile_p_ex[i].write(p_ex_inter.part(i), t)
                xdmffile_u_ex[i].write(u_ex_inter.part(i), t)
        

        # Compute error squares
        # FIXME: Should be H1 not H10 errornorm
        u_grad = Expression(grad_u_ex_str, t_=t, nu=nu, degree=degree_u+degree_rise)
        h1_errors_u.append(assemble_multimesh(inner(grad(u_h)-u_grad, grad(u_h)-u_grad)*dX))
        l2_errors_u.append(errornorm(u_ex, u_h, norm_type="L2")**2)
        l2_errors_p.append(errornorm(p_ex, p_h, norm_type="L2")**2)
    [out.close() for out in xdmffile_tent]
    [out.close() for out in xdmffile_u]
    [out.close() for out in xdmffile_p]
    [out.close() for out in xdmffile_u_ex]
    [out.close() for out in xdmffile_p_ex]
    print("Simulation ended")
    list_timings(TimingClear.clear, [TimingType.wall])
    return (h1_errors_u, l2_errors_u, l2_errors_p)

def compute_errors(st_errors, dt):
    h1_errors_u, l2_errors_u, l2_errors_p = st_errors[0], st_errors[1], st_errors[2]
    
    h1_error_at_T_u = sqrt(h1_errors_u[-1])
    l2_error_at_T_u = sqrt(l2_errors_u[-1])
    l2_error_at_T_p = sqrt(l2_errors_p[-1])
 
    max_h1_error_u = sqrt(h1_errors_u.max())
    max_l2_error_u = sqrt(l2_errors_u.max())
    max_l2_error_p = sqrt(l2_errors_p.max())
    compute_l2_time_err = lambda dt, errors :  sqrt(dt*sum(errors))
    l2_h1_error_u = compute_l2_time_err(dt, h1_errors_u)
    l2_l2_error_u = compute_l2_time_err(dt, l2_errors_u)
    l2_l2_error_p = compute_l2_time_err(dt, l2_errors_p)
  
    print("========================================")
    print("Computed errors:")
    print("----------------------------------------")
    print("H1 error_u at T = %e" % h1_error_at_T_u)
    print("Max H1 error_u = %e" % max_h1_error_u)
    print("L2 H1 error_u = %e" % l2_h1_error_u)
    print("----------------------------------------")
    print("L2 error_u at T = %e" % l2_error_at_T_u)
    print("Max L2 error_u = %e" % max_l2_error_u)
    print("L2 L2 error_u = %e" % l2_l2_error_u)
    print("----------------------------------------")
    print("L2 error_p at T = %e" % l2_error_at_T_p)
    print("Max L2 error_p = %e" % max_l2_error_p)
    print("L2 L2 error_p = %e" % l2_l2_error_p)
    print("========================================")
    outdict = {"uL2T": l2_error_at_T_u, "uH1T": h1_error_at_T_u,
               "pL2T": l2_error_at_T_p, "uL2max": max_l2_error_u,
               "uH1max": max_h1_error_u, "pL2max": max_l2_error_p,
               "uL2L2": l2_l2_error_u, "uL2H1": l2_h1_error_u,
               "pL2L2": l2_l2_error_p}
    return outdict

def plot_meshes(N):
    import matplotlib.pyplot as plt

    mesh_0, mesh_1, mesh_2 = get_meshes(N)
    plot(mesh_0, color="k", linewidth=2)
    plot(mesh_1, color="r", linewidth=2)
    plot(mesh_2, color="g", linewidth=2)
    plt.savefig("multimesh.png")

def compute_eoc(errors):
    return np.log(errors[:-1]/errors[1:])/np.log(2)

def spatial_convergence(implicit):
    nu = 0.01
    t0, T = 0, 0.5
    degrees = (2,1)
    alpha, beta, delta = 50, 10, 0.05
    num_ref_time = 4
    num_ref_space = 4
    dt_0 = 0.1
    N_0 = 8
    plot_meshes(N_0)
    if implicit:
        extra_str = "implicit"
    else:
        extra_str = "explicit"
    results_top_dir = "results_spatial_{0:s}/".format(extra_str)
    use_ritz = False
    
    time_steps = [dt_0*2**(-k) for k in reversed(range(num_ref_time+1))]
    mesh_sizes = [N_0*2**k for k in reversed(range(num_ref_space+1))]
    Nt = len(time_steps)
    Nm = len(mesh_sizes)
    M = Nt*Nm
    error_dict = {}

    args =  ((nu, t0, T, time_steps[i], mesh_sizes[j], degrees, alpha, beta, delta, use_ritz,
              results_top_dir+"/N_{}_dt_{}/".format(mesh_sizes[j], time_steps[i]), implicit)
             for i in range(Nt) for j in range(Nm))
    args_copy =  ((nu, t0, T, time_steps[i], mesh_sizes[j], degrees, alpha, beta, delta, use_ritz,
                   results_top_dir+"/N_{}_dt_{}/".format(mesh_sizes[j], time_steps[i]), implicit)
                  for i in range(Nt) for j in range(Nm))
    
    with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
        for inputs, st_errors in zip(args_copy, executor.map(IPCSSolver, *zip(*args))):
            N = list(inputs)[4]
            dt = list(inputs)[3]
            i = np.log(dt_0/dt)/np.log(2)
            j = np.log(N/N_0)/np.log(2)
            # DEBUG: Check reverse engineering for indices for error matrix
            print(i, j, N, dt)
            results_dir = list(inputs)[-1]
            print("*"*55)
            print("N: {0:d}, dt: {1:.2e}".format(N, dt))
            # Compute max and l2 errors with respect to time
            st_errors = np.array(st_errors)
        
            # Computing error for running simulation
            error_dict[i,j] = compute_errors(st_errors, dt)
    # Create dictionary containing a matrix of the errors per norm type
    out_dict = {}
    for key in error_dict.keys():
        for inner_key in error_dict[key].keys():
            out_dict[inner_key] = np.zeros((Nt, Nm))#[[None for i in range(Nt)] for j in range(Nm)]
        break
    for key in out_dict.keys():
        for i in range(Nt):
            for j in range(Nm):
                out_dict[key][i,j] = "{0:.2e}".format(error_dict[i,j][key])
    np.save(results_top_dir + "error_dict.npy", out_dict)
    # Save all errors to tables
    from latex_table import simple_table
    for key in ["uL2L2", "pL2L2", "uL2H1"]:
        print("*"*10, key, "*"*10)
        print(simple_table(out_dict[key]))
 
def temporal_convergence(implicit):
    nu = 0.01
    t0, T = 0, 0.5
    degrees = (4,3)
    alpha, beta, delta = 50, 10, 0.05
    num_ref_time = 2
    num_ref_space = 2
    dt_0 = 0.1
    N_0 = 8
    results_top_dir = "results_temporal_multimesh/"
    use_ritz = False
    
    time_steps = [dt_0*2**(-k) for k in reversed(range(num_ref_time+1))]
    mesh_sizes = [N_0*2**k for k in reversed(range(num_ref_space+1))]
    Nt = len(time_steps)
    Nm = len(mesh_sizes)
    M = Nt*Nm
    error_dict = {}

    args =  ((nu, t0, T, time_steps[i], mesh_sizes[j], degrees, alpha, beta, delta, use_ritz,
              results_top_dir+"/N_{}_dt_{}/".format(mesh_sizes[j], time_steps[i]), implicit)
             for i in range(Nt) for j in range(Nm))
    args_copy =  ((nu, t0, T, time_steps[i], mesh_sizes[j], degrees, alpha, beta, delta, use_ritz,
                   results_top_dir+"/N_{}_dt_{}/".format(mesh_sizes[j], time_steps[i]), implicit)
                  for i in range(Nt) for j in range(Nm))
    
    with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
        for inputs, st_errors in zip(args_copy, executor.map(IPCSSolver, *zip(*args))):
            N = list(inputs)[4]
            dt = list(inputs)[3]
            i = np.log(dt_0/dt)/np.log(2)
            j = np.log(N/N_0)/np.log(2)
            # DEBUG: Check reverse engineering for indices for error matrix
            print(i, j, N, dt)
            results_dir = list(inputs)[-1]
            print("*"*55)
            print("N: {0:d}, dt: {1:.2e}".format(N, dt))
            # Compute max and l2 errors with respect to time
            st_errors = np.array(st_errors)
            # Computing error for running simulation
            error_dict[i,j] = compute_errors(st_errors, dt)
    # Create dictionary containing a matrix of the errors per norm type
    out_dict = {}
    for key in error_dict.keys():
        for inner_key in error_dict[key].keys():
            out_dict[inner_key] = np.zeros((Nt, Nm))#[[None for i in range(Nt)] for j in range(Nm)]
        break
    for key in out_dict.keys():
        for i in range(Nt):
            for j in range(Nm):
                out_dict[key][i,j] = "{0:.2e}".format(error_dict[i,j][key])
    np.save(results_top_dir + "error_dict.npy", out_dict)
    # Save all errors to tables
    from latex_table import simple_table
    for key in ["uL2L2", "pL2L2", "uL2H1"]:
        print("*"*10, key, "*"*10)
        print(simple_table(out_dict[key]))


if __name__ == "__main__":
    import time

    start = time.time()
    import argparse
    parser = argparse.ArgumentParser()
    feature_parser = parser.add_mutually_exclusive_group(required=True)
    feature_parser.add_argument('--spatial', dest='spatial',
                                action='store_true')
    feature_parser.add_argument('--temporal', dest='spatial',
                                action='store_false')
    feature_parser2 = parser.add_mutually_exclusive_group(required=False)
    feature_parser2.add_argument('--implicit', dest='implicit',
                                action='store_true')
    feature_parser2.add_argument('--explicit', dest='implicit',
                                 action='store_false')
    parser.set_defaults(implicit=False)
    import sys
    thismodule = sys.modules[__name__]
    args = parser.parse_args()
    for key in vars(args):
        setattr(thismodule, key, getattr(args, key))
    if spatial:
        spatial_convergence(implicit)
    else:
        temporal_convergence(implicit)  
    end = time.time()
    print("Runtime: {0:.2e}".format(end-start))
