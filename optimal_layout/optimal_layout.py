from dolfin import *
from dolfin.cpp.mesh import MeshFunctionSizet
from create_meshes import inflow, outflow, walls, obstacle, c_x, c_y, L, H
import concurrent.futures

import numpy as np
from IPython import embed
set_log_level(LogLevel.CRITICAL)
from IPOPTSolver import PositionalOptimization, memoize

T = 1
P = 0.1
class BoundaryFunction(UserExpression):
    def __init__(self, t, **kwargs):
        super().__init__(**kwargs)
        self.t = t
        self.P = P
    def eval(self, values, x):
        if self.t >= self.P:
            U = 1
        else:
            U = sin(pi/2*self.t/self.P)
        values[0] = U
        values[1] = 0
    
    def value_shape(self):
        return (2,)

d = 0
from tqdm import tqdm
@memoize
def navier_stokes(positions, rotation, outdir=None, BDF2=False, progressbar=False):
    global d
    nu = 0.001
    degrees = (2,1)
    alpha, beta, delta = 50, 10, 0.05
    dt = 1/100
    if outdir is None:
        results_dir="results/{0:s}/".format(str(d).zfill(3))
        d +=1
    else:
        results_dir=outdir
    (degree_u, degree_p) = degrees
    f = Constant((0,0))

    # Load background mesh
    mesh_0 = Mesh()
    with XDMFFile("mesh_0.xdmf") as infile:
        infile.read(mesh_0)
    mvc = MeshValueCollection("size_t", mesh_0, 1)
    with XDMFFile("mf_0.xdmf") as infile:
        infile.read(mvc, "name_to_read")
    mf_0 = MeshFunctionSizet(mesh_0, mvc)
    mesh_1 = Mesh()
    with XDMFFile("opt_area.xdmf") as infile:
        infile.read(mesh_1)

    # Load object meshes
    mesh_X = []
    mf_X = []
    N_m = int(len(positions)/3) if rotation else int(len(positions)/2)
    mesh_pos = positions[:2*N_m].reshape(N_m, 2)
    if rotation:
        rots = positions[2*N_m:]
    else:
        rots = np.zeros(N_m)
    for pos, rot in zip(mesh_pos, rots):                 
        mesh = Mesh()
        with XDMFFile("mesh_1.xdmf") as infile:
            infile.read(mesh)
        mesh.translate(Point(*pos))
        if rotation:
            ang_rot = rot*360/(2*pi)
            mesh.rotate(ang_rot, 2, Point(*pos))
        mesh_X.append(mesh)
        mvc = MeshValueCollection("size_t", mesh, 1)
        with XDMFFile("mf_1.xdmf") as infile:
            infile.read(mvc, "name_to_read")
        mf_X.append(MeshFunctionSizet(mesh, mvc))

    # Build MultiMeshes
    multimesh = MultiMesh()
    for mesh in [mesh_0, mesh_1]+mesh_X:
        multimesh.add(mesh)
    multimesh.build(2*degree_u)

    # Cover cells in the hole of the obstacle
    for pos in mesh_pos:
        multimesh.auto_cover(1, Point(*pos))

    num_parts = multimesh.num_parts()

    # Build function spaces 
    V_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u)
    V = MultiMeshFunctionSpace(multimesh, V_el)

    Q_el = FiniteElement("CG", mesh_0.ufl_cell(), degree_p)
    Q = MultiMeshFunctionSpace(multimesh, Q_el)

    # Set-up functions
    u_h = MultiMeshFunction(V)
    u_old = MultiMeshFunction(V)
    u_s = MultiMeshFunction(V)

    p_h = MultiMeshFunction(Q)
    p_c = MultiMeshFunction(Q)

    dt_c = Constant(dt)

    # Setup for step 1: computing tentative velocity
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # N.B. Full explicit, 1st order convection add_terms
    n = FacetNormal(multimesh)
    h = 2.0*Circumradius(multimesh)

    tensor_jump = lambda v, n: outer(v('+'), n('+')) + outer(v('-'), n('-'))

    weight_time = Constant(3/(2*dt_c)) if BDF2 else Constant(1/dt_c)
    weight_diffusion = nu if BDF2 else Constant(0.5)*nu
    a1 = (weight_time*inner(u, v) + weight_diffusion*inner(grad(u),grad(v)))*dX
    L1 = (inner(p_h, div(v)) + inner(f, v))*dX
    L1 -= avg(p_h)*inner(n("-"), v("-")-v("+"))*dI
    L1 -= jump(p_h)*inner(n("-"), avg(v))*dI
    if BDF2:
        L1 += (1/(2*dt_c)*inner(4*u_h - u_old, v)
              - 2*inner(grad(u_h)*u_h, v)
              + inner(grad(u_old)*u_old, v))*dX
    else:
        u_AB = Constant(1.5)*u_h-Constant(0.5)*u_old
        a1 += Constant(0.5)*inner(grad(u)*u_AB, v)*dX
        # RHS
        L1 += (weight_time*inner(u_h, v)
              - weight_diffusion*inner(grad(u_h), grad(v))
              - 0.5*inner(grad(u_h)*u_AB, v))*dX
        # - inner(3/2*grad(u_h)*u_h-0.5*grad(u_old)*u_old, v))*dX

        L1 += weight_diffusion*(inner(dot(grad(u_h("-")), n("-")), v("-"))-
                                inner(dot(grad(u_h("+")), n("-")), v("+")))*dI
    # Nitsche terms
    a1 += weight_diffusion*(-inner(avg(grad(u)), tensor_jump(v, n))*dI
              -inner(avg(grad(v)), tensor_jump(u, n))*dI
              +alpha/avg(h)*inner(jump(u), jump(v))*dI)
    a1 += weight_diffusion*beta*inner(jump(grad(u)), jump(grad(v)))*dO
    # Stabilization of mass terms
    a1 += weight_time*beta*inner(jump(u), jump(v))*dO


    # Define boundary conditions
    t = 0
    U_inlet = BoundaryFunction(t)
    bc_inlet = MultiMeshDirichletBC(V, U_inlet, mf_0, inflow, 0)
    bc_walls = MultiMeshDirichletBC(V, Constant((0,0)), mf_0, walls, 0)
    bc_obstacles = [MultiMeshDirichletBC(V, Constant((0,0)), mf, obstacle, i+2) for i, mf in enumerate(mf_X)]
    bcs_u = [bc_inlet, bc_walls] + bc_obstacles 

    # Assemble left hand sides and apply boundary conditions
    if BDF2:
        A1 = assemble_multimesh(a1)
    #[bc_u.apply(A1) for bc_u in bcs_u]
    #bc_inlet.apply(u_h.vector())
    #bc_inlet.apply(u_old.vector())
    # Assemble L just to apply lock_inactive_dofs for A
    # lock_inactive_dofs should have additional variants
    # for only A or b
    b1 = assemble_multimesh(L1)
    
    # Setup for step 2: compute pressure update
    p = TrialFunction(Q)
    q = TestFunction(Q)
    
    a2 = inner(grad(p), grad(q))*dX

    # Nitsche terms 
    a2 += (-dot(avg(grad(p)), jump(q, n))
           -dot(avg(grad(q)), jump(p, n))
           +alpha/avg(h)*inner(jump(p), jump(q)))*dI
    
    
    # Overlap stabilization for p
    a2 += beta*inner(jump(grad(p)), jump(grad(q)))*dO
    L2 = -weight_time*inner(div(u_s), q)*dX

    A2 = assemble_multimesh(a2)
    b2 = assemble_multimesh(L2)
    bc_p0 = MultiMeshDirichletBC(Q, Constant(0), mf_0, outflow, 0)
    bcs_p = [bc_p0]
    # create solver and factorize matrix
    solver_2 = LUSolver(A2, "mumps")

    # Setup for step 3: update velocity
    a3 = inner(u, v)*dX
    # Overlap stabilization 
    # also cover dI related stabilizations
    a3 += beta*inner(jump(u), jump(v))*dO
    L3 = (inner(u_s, v) - 1/weight_time*inner(grad(p_c), v))*dX
    A3 = assemble_multimesh(a3)
    b3 = assemble_multimesh(L3)
    V.lock_inactive_dofs(A3, b3)
    
    # create solver and factorize matrix
    solver_3 = LUSolver(A3, "mumps")

    # Define output files and write out data for initial step
    if outdir is not None:
        file_u = [File(results_dir + "u_part_{0:d}.pvd".format(i))
                  for i in range(num_parts)]
        file_p = [File(results_dir + "p_part_{0:d}.pvd".format(i))
                  for i in range(num_parts)]
        for i in range(num_parts):
            file_u[i].write(u_h.part(i), t)
            file_p[i].write(p_h.part(i), t)

    # Compute reference quantities
    ns = [-FacetNormal(mesh) for mesh in mesh_X] 
    dObs = [Measure("ds", domain=mesh,
                   subdomain_data=mf, subdomain_id=obstacle) for mesh, mf in zip(mesh_X, mf_X)]
    J = 0
    if progressbar:
        progress = tqdm(desc="Solving state", total=int(T/dt))
    # Solve problem
    while(t <= T-dt*10e-3):
        if progressbar:
            progress.update(1)
        # Increase time and update data
        t += dt
        U_inlet.t = t

        # Step 1: compute tentative velocity 
        b1 = assemble_multimesh(L1)
        if not BDF2:
            A1 = assemble_multimesh(a1)
        [bc_u.apply(A1,b1) for bc_u in bcs_u]
        V.lock_inactive_dofs(A1, b1)
        solver_1 = LUSolver(A1, "mumps")
        solver_1.solve(u_s.vector(), b1)
        
        # Step 2: compute pressure correction
        b2 = assemble_multimesh(L2)
        Q.lock_inactive_dofs(A2, b2)
        [bc_p.apply(A2, b2) for bc_p in bcs_p]
        solver_2.solve(p_c.vector(), b2)

        # Update pressure
        p_h.vector().axpy(1.0, p_c.vector())

        # Write out computed p
        if outdir is not None:
            for i in range(num_parts):
                file_p[i].write(p_h.part(i), t)

        # Assign new u_old
        u_old.assign(u_h)
    
        # Step 3: compute velocity update
        b3 = assemble_multimesh(L3)
        V.lock_inactive_dofs(A3, b3)
        solver_3.solve(u_h.vector(), b3)
        # Write out computed u
        if outdir is not None:
            for i in range(num_parts):
                file_u[i].write(u_h.part(i), t)

        # Compute the drag contribution of all obstacles
        for i in range(num_parts-2):
            p_i = p_h.part(i+2, deepcopy=True)
            u_i = u_h.part(i+2, deepcopy=True)
            u_t = inner(as_vector((ns[i][1], -ns[i][0])), u_i)
            drag = 2/0.1*(nu*inner(grad(u_t), ns[i])*ns[i][1] - p_i*ns[i][0])*dObs[i]
            if t > P:
                J += assemble(dt*drag)        
    # [out.close() for out in xdmffile_u]
    # [out.close() for out in xdmffile_p]
    J = -J
    # print("J: {0:.7e}".format(J), *("{0:.3f}, {1:.3f}, ".format(xi,yi) for
    #                                  (xi,yi) in mesh_pos), end="")
    # if rotation:
    #     print(*("{0:.3f}".format(rot) for rot in rots))
    # else:
    #     print("")
    return J

def hook(positions, rotation, mesh_files=None, outdir=None, progressbar=True):
    navier_stokes.__wrapped__(positions, rotation, outdir=outdir, progressbar=progressbar)
    N_m = int(len(positions)/3) if rotation else int(len(positions)/2)
    num_mesh = N_m + 2
    if mesh_files is None:
        mesh_files = [File("results/outmesh{0:s}.pvd".format(str(j).zfill(3))) for j in range(num_mesh)]
    mesh_0 = Mesh()
    mesh_1 = Mesh()
    meshes = [mesh_0, mesh_1]
    with XDMFFile("mesh_0.xdmf") as infile:
        infile.read(mesh_0)
    with XDMFFile("opt_area.xdmf") as infile:
        infile.read(mesh_1)

    mesh_pos = positions[:2*N_m].reshape(int(N_m), 2)
    if rotation:
        rots = positions[2*N_m:]
    else:
        rots = numpy.ones(N_m) # Dummy variable if not rotation

    for pos, rot in zip(mesh_pos, rots):
        mesh = Mesh()
        meshes.append(mesh)
        with XDMFFile("mesh_1.xdmf") as infile:
            infile.read(mesh)
        mesh.translate(Point(*pos))
        if rotation:
            ang_rot = rot*360/(2*pi)
            mesh.rotate(ang_rot, 2, Point(*pos))
            
    for j in range(num_mesh):
        mesh_files[j].write(meshes[j])
    return mesh_files

out_dict={}
def cb(algmod, iter_count, obj_value, inf_pr, inf_du, 
                          mu, d_norm, regularization_size, alpha_du, alpha_pr,
                          ls_trials, user_data=None): 
    out_dict[iter_count] = obj_value
    return True

if __name__ == "__main__":
    import time
    start = time.time()
    rotation = True
    if rotation:
        pos = np.array([0.41,0.4*H, 0.61,0.4*H, 0.81,0.4*H,
                        0.41,0.6*H, 0.61,0.6*H, 0.81,0.6*H,
                        pi/2, pi/2, pi/2, pi/2, pi/2, pi/2])    
    else:
        pos = np.array([0.41, H/2, 0.61,H/2, 0.81,H/2, 1.01,H/2])
    # To check that we get a sensible init result
    init_files = hook(pos, rotation, outdir="results/init/", progressbar=True)

    opt_solver = PositionalOptimization(6, navier_stokes,
                                        max_iter=100,
                                        eps=1e-3,
                                        print_lvl=5, cb=cb,
                                        max_workers=5, rotation=rotation)
    opt_x, zl, zu, obj, status, zg = opt_solver.nlp.solve(pos)
    print("init:", navier_stokes(pos, rotation))
    print("Final:", navier_stokes(opt_x, rotation), obj)
    np.save("results/iterations", out_dict)

    hook(opt_x, rotation, init_files, outdir="results/final/", progressbar=True)

    end = time.time()
    print("Runtime: {0:.2f}".format(end-start))
