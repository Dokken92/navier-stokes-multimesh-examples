import numpy
from dolfin import *
from dolfin.cpp.mesh import MeshFunctionSizet
import pyipopt
from IPython import embed
import sympy
from matplotlib import pylab as plt
from create_meshes import L, H, dist, ref_x, ref_y, l, h, outer_r
import concurrent.futures
from tqdm import tqdm

from functools import wraps
def memoize(f, **kwargs):
    memo = {}
    @wraps(f)
    def helper(x, rotation, **kwargs):
        if hash(x.tostring()) not in memo:
            memo[hash(x.tostring())] = f(x, rotation, **kwargs)
        return memo[hash(x.tostring())]
    return helper

class PositionalOptimization():

    def __init__(self, num_obj, J, max_iter=20, eps=1e-5, print_lvl=5, cb=None,
                 max_workers=6, rotation=False):
        self.max_workers=max_workers
        self.eps_dist = 1e-3
        self.eps = eps
        self.max_iter = max_iter
        self.print_lvl = print_lvl
        self.g_scale = 1
        self.num_obj = num_obj
        self.rotation = rotation
        self.sympy_g()
        self.sympy_jac_g()
        self.solve_init(J, cb=cb)
        self.eval_J = J
        self.rot_scale = 1



    def sympy_g(self):
        """ Creates the  no-collision constraint 
        for each object with sympy, returning the g in inequality g<=0 and
        an array z of the control variables
        """
        x = list(sympy.symbols("x0:%d" % self.num_obj))
        y = list(sympy.symbols("y0:%d" % self.num_obj))
        if self.rotation:
            theta = list(sympy.symbols("theta0:%d" % self.num_obj))
        g,z= [],[]
        # No-collision constraints
        for i in range(self.num_obj):
            z.append(x[i])
            z.append(y[i])
            for j in range(i+1,self.num_obj):
                xi, yi = x[i], y[i]
                xj, yj = x[j], y[j]
                g.append(self.eps_dist+dist**2 - (xi - xj)**2 - (yi - yj)**2)
        if self.rotation:
            for i in range(self.num_obj):
                z.append(theta[i])

        self.g = sympy.Matrix(g)
        self.z = sympy.Matrix(z)

    def eval_g(self, x):
        """ Evaluate inequality constraint, g(x) <= 0, """
        g_numpy = self.g.subs([(self.z[i], x[i])
                               for i in range(2*self.num_obj)])
        return self.g_scale*numpy.array(g_numpy).astype(float)
    
    def sympy_jac_g(self):
        """ Creates jacobian for constraints """
        self.jac_g = self.g.jacobian(self.z)

    def eval_jac_g(self, positions, flag):
        """ The constraint Jacobian:
        flag = True  means 'tell me the sparsity pattern';
        flag = False means 'give me the Jacobian'.
        """
        if flag:
            # FIXME: Pass in a dense matrix
            # (could be optimised, but we don't care for this small problem).
            if self.rotation:
                nvar = 3*self.num_obj
            else:
                nvar = 2*self.num_obj
            ncon = int(self.num_obj*(self.num_obj-1)/2)
            rows = []
            for i in range(ncon):
                rows += [i] * nvar
            cols = list(range(nvar)) * ncon
            return (numpy.array(rows), numpy.array(cols))
        else:
            numpy_jac_g = self.jac_g.subs([(self.z[i], positions[i])
                                           for i in range(2*self.num_obj)])
            return self.g_scale*numpy.array(numpy_jac_g).astype(float)


    def FD_dJ(self,positions):
        """ Finite difference approximation of """
        perturbation = positions.copy()
        all_perturbations = []
        for i in range(len(perturbation)+1):
            all_perturbations.append(numpy.array(perturbation))
        for i in range(1,len(perturbation)+1):
            if self.rotation:
                eps = self.eps if i<=int(2*len(perturbation)/3) else self.rot_scale*self.eps
            else:
                eps = self.eps
            all_perturbations[i][i-1] += eps
        J0 = 0
        dJ = numpy.zeros(len(positions))
        args = ((all_perturbations[i][:], self.rotation) for i in range(len(perturbation)+1))
        args_c = ((all_perturbations[i][:], self.rotation) for i in range(len(perturbation)+1))
        # progress = tqdm(desc="Evaluating gradient", total=int(len(all_perturbations)))
        with concurrent.futures.ProcessPoolExecutor(max_workers=self.max_workers) as executor:
            for input, Ji in zip(args_c,
                                 executor.map(self.eval_J,
                                              *zip(*args))):
                # progress.update(1)
                index = numpy.argmax(input[0]-perturbation)
                if numpy.max(input[0] - perturbation) == 0:
                    J0 = Ji
                else:
                    dJ[index] = Ji

        dJ -= J0
        if self.rotation:
            for i in range(1,len(perturbation)+1):
                eps = self.eps if i<=int(2*len(perturbation)/3) else self.rot_scale*self.eps
                dJ[i-1] /= eps
        else:
            dJ /= self.eps
        mesh_pos = positions.reshape(int(len(positions)/2), 2)
        print("J: {0:.5e}, dJ:".format(J0), *("{0:6.1e}".format(dJi) for dJi in dJ))
        print("at:", *("{0:6.5f} {1:6.5f}".format(xi,yi) for
                        (xi,yi) in mesh_pos))
        return dJ

    
    def solve_init(self, eval_J, cb=None):
        if self.rotation:
            nvar = int(3*self.num_obj)
        else:
            nvar = int(2*self.num_obj)
        ncon = int(self.num_obj*(self.num_obj-1)/2)
        low_var = numpy.tile([ref_x+abs(outer_r), ref_y+abs(outer_r)], self.num_obj)
        up_var = numpy.tile([ref_x+l-abs(outer_r),ref_y+h-abs(outer_r)], self.num_obj)
        if self.rotation:
            low_var = numpy.append(low_var, -0.1*numpy.ones(self.num_obj))
            up_var = numpy.append(up_var, (pi+0.1)*numpy.ones(self.num_obj))

        inf_con = numpy.inf*numpy.ones(ncon, dtype=float)
        zero_con = numpy.zeros(ncon, dtype=float)
        self.nlp = pyipopt.create(nvar,      # Number of controls
                                  low_var,  # Lower bounds for Control
                                  up_var,   # Upper bounds for Control
                                  ncon,      # Number of constraints
                                  -inf_con,  # Lower bounds for contraints
                                  zero_con,   # Upper bounds for contraints
                                  nvar*ncon, # Number of nonzeros in cons. Jac
                                  0,         # Number of nonzeros in cons. Hes
                                  lambda pos: eval_J(pos, self.rotation,
                                                     progressbar=False),  # Objective eval
                                  lambda pos: self.FD_dJ(pos), # Obj. grad eval
                                  self.eval_g,    # Constraint evaluation
                                  self.eval_jac_g # Constraint Jacobian evaluation
        )
         # So it does not violate the boundary constraint
        self.nlp.num_option('bound_relax_factor', 0)
        self.nlp.str_option('mu_strategy', 'adaptive')
        self.nlp.int_option('print_level', self.print_lvl)
        self.nlp.int_option('max_iter', self.max_iter)
        self.nlp.num_option('tol', 1e-2)
        # self.nlp.num_option('acceptable_tol', 1e-1)
        # self.nlp.num_option('constr_viol_tol', 1e-1)
        if cb is not None:
            self.nlp.set_intermediate_callback(cb)

        
d=0
@memoize
def test_J(positions, rotation, plot_fig=False, progressbar=False):
    global d
    # Load background mesh
    mesh_0 = Mesh()
    if rotation:
        N_m = int(len(positions)/3)
    else:
        N_m = int(len(positions)/2)
    with XDMFFile("mesh_0.xdmf") as infile:
        infile.read(mesh_0)
    mvc = MeshValueCollection("size_t", mesh_0, 1)
    with XDMFFile("mf_0.xdmf") as infile:
        infile.read(mvc, "name_to_read")
    mf_0 = MeshFunctionSizet(mesh_0, mvc)
    if plot_fig:
        plt.figure()
        plot(mesh_0, linewidth=0.33)
    mesh_0 = Mesh()
    with XDMFFile("opt_area.xdmf") as infile:
        infile.read(mesh_0)
    if plot_fig:
        plot(mesh_0, color="k", linewidth=0.33)

    # Load object meshes
    mesh_X = []
    mf_X = []
    mesh_pos = positions[:2*N_m].reshape(N_m, 2)
    if rotation:
        rots = positions[2*N_m:]
    else:
        rots = numpy.ones(N_m) # Dummy variable if not rotation
    if plot_fig:
        for (pos, rot, color) in zip(mesh_pos, rots, ["r","b", "g", "y", "m", "c"]):
            mesh = Mesh()
            with XDMFFile("mesh_1.xdmf") as infile:
                infile.read(mesh)
            mesh.translate(Point(*pos))
            if rotation:
                ang_rot = rot*360/(2*pi)
                mesh.rotate(ang_rot, 2, Point(*pos))
            mesh_X.append(mesh)
      
            plot(mesh,color=color, linewidth=0.33)
        plt.savefig("results_test/test_opt{0:s}.png".format(str(d).zfill(3)), dpi=500)
        d+=1

    """ Test of optimization without state eq"""
    j = 0.5*numpy.dot(positions, positions)
    if rotation:
        j = 0.5*numpy.dot(positions[:2*N_m], positions[:2*N_m])
        j += 0.5*numpy.dot(positions[2*N_m:]-pi/4*numpy.ones(N_m),
                           positions[2*N_m:]-pi/4*numpy.ones(N_m))
    return j

if __name__ == "__main__":
    rotation = True
    if rotation:
        init = numpy.array([0.4, H/2, 0.6,H/2, 0.8,H/2, 1,H/2, pi/5,pi/4,pi/3,pi/2])
    else:
        init = numpy.array([0.4, H/2, 0.6,H/2, 0.8,H/2, 1,H/2])
    opt_solver = PositionalOptimization(4, test_J, eps=1e-2, max_iter=50, rotation=rotation)
    import os
    if not os.path.exists("results_test"):
        os.makedirs("results_test")
    else:
        os.system("rm results_test/*")
    solution = opt_solver.nlp.solve(init)
    test_J.__wrapped__(init, rotation=rotation,plot_fig=True)
    test_J.__wrapped__(solution[0], rotation=rotation, plot_fig=True)
