from dolfin import Mesh, XDMFFile, Point, plot, pi
from dolfin.cpp.mesh import MeshFunctionSizet
import numpy
from create_meshes import H
from matplotlib import pylab as plt

d_viz=0
def visualize(positions):
    global d_viz
    N_m = int(len(positions)/3)
    # Load background mesh
    mesh_0 = Mesh()
    with XDMFFile("mesh_0.xdmf") as infile:
        infile.read(mesh_0)
    plt.figure()
    plot(mesh_0, linewidth=0.33)
    opt_area = Mesh()
    with XDMFFile("opt_area.xdmf") as infile:
        infile.read(opt_area)
    plot(opt_area, color="k", linewidth=0.33)
    # Load object meshes
    mesh_X = []
    mf_X = []
    mesh_pos = positions[:2*N_m].reshape(N_m, 2)
    rots = positions[2*N_m:]

    for pos, rot, col in zip(mesh_pos, rots, ["r","g","b","y"]):
        mesh = Mesh()
        with XDMFFile("mesh_1.xdmf") as infile:
            infile.read(mesh)
        mesh.translate(Point(*pos))
        ang_rot = rot*360/(2*pi)
        mesh.rotate(ang_rot, 2, Point(*pos))
        mesh_X.append(mesh)
        plot(mesh,color=col, linewidth=0.33)

    plt.savefig("results/track_iter{0:s}.png".format(str(d_viz).zfill(3)), dpi=500)
    d_viz+=1

if __name__ == "__main__":
    # visualize(numpy.array([]))
    visualize(numpy.array([0.41, H/2, 0.61,H/2, 0.81,H/2, 1.01,H/2, pi/2, pi/2, pi/2, pi/2]))
    # 5
    visualize(numpy.array([ 0.44646 ,0.53604 ,0.56919 ,0.84317 ,0.95492 ,0.93589, 1.02770 ,0.62941, 1.61197 ,1.63014, 1.60749, 1.52810]))
    # 18
    visualize(numpy.array([0.40215 ,0.63901 ,0.40284 ,0.82346 ,0.67667 ,0.99995 ,0.97529 ,0.50220, 2.98594, 3.13444, 2.81625 ,0.36091]))
    # 19
    visualize(numpy.array([ 0.41517 ,0.64320 ,0.43366 ,0.82969 ,0.59670 ,0.99474 ,0.93502 ,0.50906, 2.86123, 3.11933 ,2.67990 ,0.49034]))
    # 20
    visualize(numpy.array([ 0.45041 ,0.61653 ,0.45669 ,0.82324 ,0.66659 ,0.98391 ,0.98916 ,0.56221, 2.90451, 2.92024, 2.72309 ,0.36636]))
    # 21
    visualize(numpy.array([0.45026 ,0.57255 ,0.49451 ,0.77488 ,0.67631 ,0.97396 ,1.08474 ,0.56304, 2.91820, 2.99910, 2.70485 ,0.37545]))
    # 22
    visualize(numpy.array([     0.45838 ,0.56633 ,0.47112 ,0.77552 ,0.65063 ,0.97300, 1.07689 ,0.56549, 2.92077, 2.99106, 2.70060 ,0.38520]))

