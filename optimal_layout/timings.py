from dolfin import *
from dolfin.cpp.mesh import MeshFunctionSizet
from create_meshes import inflow, outflow, walls, obstacle, c_x, c_y, L, H

import numpy as np
from IPython import embed
set_log_level(LogLevel.ERROR)
T = 1
P = 0.1
class BoundaryFunction(UserExpression):
    def __init__(self, t, **kwargs):
        super().__init__(**kwargs)
        self.t = t
        self.P = P
    def eval(self, values, x):
        if self.t >= self.P:
            U = 1
        else:
            U = sin(pi/2*self.t/self.P)
        values[0] = U
        values[1] = 0
    
    def value_shape(self):
        return (2,)

def navier_stokes(positions, rotation, BDF2=False):
    nu = 0.001
    degrees = (2,1)
    alpha, beta, delta = 50, 10, 0.05
    dt = 1/100
    (degree_u, degree_p) = degrees
    f = Constant((0,0))

    # Load background mesh
    mesh_0 = Mesh()
    with XDMFFile("mesh_0.xdmf") as infile:
        infile.read(mesh_0)
    mvc = MeshValueCollection("size_t", mesh_0, 1)
    with XDMFFile("mf_0.xdmf") as infile:
        infile.read(mvc, "name_to_read")
    mf_0 = MeshFunctionSizet(mesh_0, mvc)
    mesh_1 = Mesh()
    with XDMFFile("opt_area.xdmf") as infile:
        infile.read(mesh_1)

    # Load object meshes
    mesh_X = []
    mf_X = []
    N_m = int(len(positions)/3) if rotation else int(len(positions)/2)
    mesh_pos = positions[:2*N_m].reshape(N_m, 2)
    if rotation:
        rots = positions[2*N_m:]
    else:
        rots = np.zeros(N_m)
    for pos, rot in zip(mesh_pos, rots):                 
        mesh = Mesh()
    with XDMFFile("mesh_1.xdmf") as infile:
        infile.read(mesh)
    with Timer("Mesh update: move"):
        mesh.translate(Point(*pos))
        if rotation:
            ang_rot = rot*360/(2*pi)
            mesh.rotate(ang_rot, 2, Point(*pos))
    mesh_X.append(mesh)
    mvc = MeshValueCollection("size_t", mesh, 1)
    with XDMFFile("mf_1.xdmf") as infile:
        infile.read(mvc, "name_to_read")
    mf_X.append(MeshFunctionSizet(mesh, mvc))

    # Build MultiMeshes
    multimesh = MultiMesh()
    for mesh in [mesh_0, mesh_1]+mesh_X:
        multimesh.add(mesh)

    with Timer("Mesh update: intersections"):
        multimesh.build(2*degree_u)

    # Cover cells in the hole of the obstacle
    with Timer("Mesh update: cover"):
        for pos in mesh_pos:
            multimesh.auto_cover(1, Point(*pos))

    num_parts = multimesh.num_parts()

    # Build function spaces 
    V_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u)
    V = MultiMeshFunctionSpace(multimesh, V_el)

    Q_el = FiniteElement("CG", mesh_0.ufl_cell(), degree_p)
    Q = MultiMeshFunctionSpace(multimesh, Q_el)

    # Set-up functions
    u_h = MultiMeshFunction(V)
    u_old = MultiMeshFunction(V)
    u_s = MultiMeshFunction(V)

    p_h = MultiMeshFunction(Q)
    p_c = MultiMeshFunction(Q)

    dt_c = Constant(dt)

    # Setup for step 1: computing tentative velocity
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # N.B. Full explicit, 1st order convection add_terms
    n = FacetNormal(multimesh)
    h = 2.0*Circumradius(multimesh)

    tensor_jump = lambda v, n: outer(v('+'), n('+')) + outer(v('-'), n('-'))

    weight_time = Constant(3/(2*dt_c)) if BDF2 else Constant(1/dt_c)
    weight_diffusion = nu if BDF2 else Constant(0.5)*nu
    a1 = (weight_time*inner(u, v) + weight_diffusion*inner(grad(u),grad(v)))*dX
    L1 = (inner(p_h, div(v)) + inner(f, v))*dX
    L1 -= avg(p_h)*inner(n("-"), v("-")-v("+"))*dI
    L1 -= jump(p_h)*inner(n("-"), avg(v))*dI
    if BDF2:
        L1 += (1/(2*dt_c)*inner(4*u_h - u_old, v)
              - 2*inner(grad(u_h)*u_h, v)
              + inner(grad(u_old)*u_old, v))*dX
    else:
        u_AB = Constant(1.5)*u_h-Constant(0.5)*u_old
        a1 += Constant(0.5)*inner(grad(u)*u_AB, v)*dX
        # RHS
        L1 += (weight_time*inner(u_h, v)
              - weight_diffusion*inner(grad(u_h), grad(v))
              - 0.5*inner(grad(u_h)*u_AB, v))*dX
        # - inner(3/2*grad(u_h)*u_h-0.5*grad(u_old)*u_old, v))*dX

        L1 += weight_diffusion*(inner(dot(grad(u_h("-")), n("-")), v("-"))-
                                inner(dot(grad(u_h("+")), n("-")), v("+")))*dI
    # Nitsche terms
    a1 += weight_diffusion*(-inner(avg(grad(u)), tensor_jump(v, n))*dI
              -inner(avg(grad(v)), tensor_jump(u, n))*dI
              +alpha/avg(h)*inner(jump(u), jump(v))*dI)
    a1 += weight_diffusion*beta*inner(jump(grad(u)), jump(grad(v)))*dO
    # Stabilization of mass terms
    a1 += weight_time*beta*inner(jump(u), jump(v))*dO


    # Define boundary conditions
    t = 0
    U_inlet = BoundaryFunction(t)
    bc_inlet = MultiMeshDirichletBC(V, U_inlet, mf_0, inflow, 0)
    bc_walls = MultiMeshDirichletBC(V, Constant((0,0)), mf_0, walls, 0)
    bc_obstacles = [MultiMeshDirichletBC(V, Constant((0,0)), mf, obstacle, i+2) for i, mf in enumerate(mf_X)]
    bcs_u = [bc_inlet, bc_walls] + bc_obstacles 

    # Assemble left hand sides and apply boundary conditions
    if BDF2:
        A1 = assemble_multimesh(a1)
    #[bc_u.apply(A1) for bc_u in bcs_u]
    #bc_inlet.apply(u_h.vector())
    #bc_inlet.apply(u_old.vector())
    # Assemble L just to apply lock_inactive_dofs for A
    # lock_inactive_dofs should have additional variants
    # for only A or b
    b1 = assemble_multimesh(L1)
    
    # Setup for step 2: compute pressure update
    p = TrialFunction(Q)
    q = TestFunction(Q)
    
    a2 = inner(grad(p), grad(q))*dX

    # Nitsche terms 
    a2 += (-dot(avg(grad(p)), jump(q, n))
           -dot(avg(grad(q)), jump(p, n))
           +alpha/avg(h)*inner(jump(p), jump(q)))*dI
    
    
    # Overlap stabilization for p
    a2 += beta*inner(jump(grad(p)), jump(grad(q)))*dO
    L2 = -weight_time*inner(div(u_s), q)*dX
    with Timer("Assemble 2: LHS"):
        A2 = assemble_multimesh(a2)
    b2 = assemble_multimesh(L2)
    bc_p0 = MultiMeshDirichletBC(Q, Constant(0), mf_0, outflow, 0)
    bcs_p = [bc_p0]
    # create solver and factorize matrix
    solver_2 = LUSolver(A2, "mumps")

    # Setup for step 3: update velocity
    a3 = inner(u, v)*dX
    # Overlap stabilization 
    # also cover dI related stabilizations
    a3 += beta*inner(jump(u), jump(v))*dO
    L3 = (inner(u_s, v) - 1/weight_time*inner(grad(p_c), v))*dX
    with Timer("Assemble 3: LHS"):
        A3 = assemble_multimesh(a3)

    b3 = assemble_multimesh(L3)
    V.lock_inactive_dofs(A3, b3)
    
    # create solver and factorize matrix
    solver_3 = LUSolver(A3, "mumps")


    # Compute reference quantities
    ns = [-FacetNormal(mesh) for mesh in mesh_X] 
    dObs = [Measure("ds", domain=mesh,
                   subdomain_data=mf, subdomain_id=obstacle) for mesh, mf in zip(mesh_X, mf_X)]
    J = 0
    # Solve problem
    while(t <= T-dt*10e-3):
        # Increase time and update data
        t += dt
        U_inlet.t = t

        # Step 1: compute tentative velocity

        with Timer("Assemble 1: RHS"):
            b1 = assemble_multimesh(L1)
        with Timer("Assemble 1: LHS"):
            if not BDF2:
                A1 = assemble_multimesh(a1)
        with Timer("Assemble 1: BCS"):
            [bc_u.apply(A1,b1) for bc_u in bcs_u]

        with Timer("Assemble 1: Lock"):
            V.lock_inactive_dofs(A1, b1)
        solver_1 = LUSolver(A1, "mumps")
        with Timer("Solve 1"):
            solver_1.solve(u_s.vector(), b1)
        
        # Step 2: compute pressure correction
        with Timer("Assemble 2: RHS"):
            b2 = assemble_multimesh(L2)
        with Timer("Assemble 2: Lock"):
            Q.lock_inactive_dofs(A2, b2)
        with Timer("Assemble 2: BCS"):
            [bc_p.apply(A2, b2) for bc_p in bcs_p]
        with Timer("Solve 2"):
            solver_2.solve(p_c.vector(), b2)

        # Update pressure
        p_h.vector().axpy(1.0, p_c.vector())

        # Assign new u_old
        u_old.assign(u_h)
    
        # Step 3: compute velocity update
        with Timer("Assemble 3: RHS"):
            b3 = assemble_multimesh(L3)
        with Timer("Assemble 3: Lock"):
            V.lock_inactive_dofs(A3, b3)
        with Timer("Solve 3"):
            solver_3.solve(u_h.vector(), b3)

        # Compute the drag contribution of all obstacles
        for i in range(num_parts-2):
            p_i = p_h.part(i+2, deepcopy=True)
            u_i = u_h.part(i+2, deepcopy=True)
            u_t = inner(as_vector((ns[i][1], -ns[i][0])), u_i)
            drag = 2/0.1*(nu*inner(grad(u_t), ns[i])*ns[i][1] - p_i*ns[i][0])*dObs[i]
            if t > P:
                J += assemble(dt*drag)        
    J = -J
    return J



if __name__ == "__main__":
    import time
    rotation = True
    pos = np.array([0.41,0.4*H, 0.61,0.4*H, 0.81,0.4*H,
                    0.41,0.6*H, 0.61,0.6*H, 0.81,0.6*H,
                    pi/2, pi/2, pi/2, pi/2, pi/2, pi/2])
    num_runs = 5
    for i in range(num_runs):
        navier_stokes(pos, rotation)

    keys = ["Mesh update: move",
            "Mesh update: intersections",
            "Mesh update: cover",
            "Assemble 1: LHS",
            "Assemble 1: RHS",
            "Assemble 1: BCS",
            "Assemble 1: Lock",
            "Assemble 2: LHS",
            "Assemble 2: RHS",
            "Assemble 2: BCS",
            "Assemble 2: Lock",
            "Assemble 3: LHS",
            "Assemble 3: RHS",
            "Assemble 3: Lock",
            "Solve 1",
            "Solve 2",
            "Solve 3"]
    print("{0:26s} | {1:5s} | {2:10s} | {3:10s}".format("Task", "Count", "Time", "Avg"))
    for key in keys:
        count, wall = timing(key, TimingClear.keep)[:2]
        print("{0:26s} {1:5d}    {2:12.8f} {3:10.8f}".format(key, count, wall, wall/count))
    ass = [0,0,0]
    atot = [0,0,0]
    for i in range(3):
        for key in ["Assemble {0:d}: LHS".format(i+1),
                    "Assemble {0:d}: RHS".format(i+1),
                    "Assemble {0:d}: BCS".format(i+1),
                    "Assemble {0:d}: Lock".format(i+1)]:
            if key in "Assemble 3: BCS":
                continue
            else:
                count, wall = timing(key, TimingClear.keep)[:2]
                ass[i] += wall/count
                atot[i] +=wall
    sss = [0,0,0]
    stot = [0,0,0]
    for i in range(1,4):
        key = "Solve {0:d}".format(i)
        count, wall = timing(key, TimingClear.keep)[:2]
        sss[i-1] = wall/count
        stot[i-1] = wall

    count, wall = timing("Mesh update: move", TimingClear.keep)[:2]
    mesh_move = wall/count
    count, wall = timing("Mesh update: intersections", TimingClear.keep)[:2]
    mesh_mm = wall/count
    #count, wall = timing("Mesh update: cover", TimingClear.keep)[:2]
    #mesh_mm += wall/count
    numbers = [ass[0], sss[0], ass[1],sss[1], ass[2], sss[2], mesh_move, mesh_mm]
    
    def latex_float(f):
        float_str = "{0:.2e}".format(f)
        base, exponent = float_str.split("e")
        return "${0} \\cdot 10^{{{1}}}$".format(base, int(exponent))
    latex_nums = ["\\textbf{One call}"]
    for number in numbers:
        latex_nums.append(latex_float(number))
    
    from tabulate import tabulate
    d = [["", "\\textbf{Step 1}", "", "\\textbf{Step 2}","", "\\textbf{Step 3}", "", "\\textbf{Mesh Update}"],
         ["\\textbf{Operation}","Assembly (s)", "Solve (s)", "Assembly (s)", "Solve (s)",
          "Assembly (s)", "Solve (s)","Update (s)", "Intersections (s)"],
         latex_nums,
         ["\\textbf{Total}", latex_float((atot[0]+stot[0])/num_runs),"",
          latex_float((atot[1]+stot[1])/num_runs),"",latex_float((atot[2]+stot[2])/num_runs),"",latex_float((mesh_move+mesh_mm)/num_runs)]]
    print(tabulate(d, tablefmt="latex_raw",stralign="center", headers="firstrow"), file=open("timings_raw.tex","w"))
    # list_timings(TimingClear.clear, [TimingType.wall])
