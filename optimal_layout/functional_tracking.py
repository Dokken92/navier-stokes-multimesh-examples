import numpy
import matplotlib.pyplot as plt
plt.rcParams['xtick.labelsize']=12
plt.rcParams['ytick.labelsize']=12
plt.rcParams['axes.labelsize']=15
plt.rcParams['axes.labelsize']=15

infile = numpy.load("results/iterations.npy")
values = infile.item()

lists = sorted(values.items()) # sorted by key, return a list of tuples

x, y = zip(*lists) # unpack a list of pairs into two tuples
plt.xlabel("Ipopt iterations")
plt.ylabel("Functional value")
plt.grid()
plt.plot(numpy.array(x), -1*numpy.array(y))
plt.savefig("J_iterations.png")


