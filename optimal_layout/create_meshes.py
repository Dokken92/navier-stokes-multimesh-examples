from pygmsh import generate_mesh
from pygmsh.built_in.geometry import Geometry
import meshio

inflow = 1
outflow = 2
walls = 3
obstacle = 4

L = 2
H = 1.5
c_x,c_y = 0.0,0.0
r_x, r_y = 0.025, 0.05
d_ = r_y
outer_r = max(r_x, r_y) + d_

# scale = 15
# span_wing = scale*0.0025
# width_wing = scale*0.0002
# body_radiusx = scale*0.001
# body_radiusy = scale*0.0007
# neck_length = scale*0.002
# neck_width = scale*0.0003
# head_radius = scale*0.0005
# beak_length = scale*0.0002
# outer_r = 1.4*(-body_radiusx-width_wing-neck_length-2*head_radius-beak_length)


dist = 1.8*outer_r
ref_x = 0.3
l = 0.9
ref_y = 0.4
h = 0.7

def refined_mesh(res=0.025):
    geometry = Geometry()
    rectangle = geometry.add_rectangle(ref_x, ref_x+l, ref_y, ref_y+h, 0, res)
    (points, cells, point_data,
     cell_data, field_data) = generate_mesh(geometry, prune_z_0=True,
                                            geo_filename="opt_area.geo")
    meshio.write("opt_area.xdmf", meshio.Mesh(
        points=points, cells={"triangle": cells["triangle"]}))

def channel_mesh(res=0.025):
    """ 
    Creates a unstructured rectangular mesh
    """
    geometry = Geometry()
    rectangle = geometry.add_rectangle(0,L,0,H,0, 2*res)

    lines = rectangle.line_loop.lines
    outflow_list = [lines[1]]
    flow_list = [lines[-1]]
    wall_list = [lines[0], lines[2]]

    geometry.add_physical_surface(rectangle.surface,label=12)
    geometry.add_physical_line(flow_list, label=inflow)
    geometry.add_physical_line(outflow_list, label=outflow)
    geometry.add_physical_line(wall_list, label=walls)
    (points, cells, point_data,
     cell_data, field_data) = generate_mesh(geometry, prune_z_0=True,
                                            geo_filename="mesh_0.geo")
    
    meshio.write("mesh_0.xdmf", meshio.Mesh(
        points=points, cells={"triangle": cells["triangle"]}))
    
    meshio.write("mf_0.xdmf", meshio.Mesh(
        points=points, cells={"line": cells["line"]},
        cell_data={"line": {"name_to_read":
                            cell_data["line"]["gmsh:physical"]}}))

def obstacle_mesh(res=0.025, d=0.01):
    geometry = Geometry()
    c = geometry.add_point((c_x,c_y,0))    

    # Elliptic obstacle
    p1 = geometry.add_point((c_x-r_x, c_y,0),lcar=0.5*res)
    p2 = geometry.add_point((c_x, c_y+r_y,0),lcar=0.5*res)
    p3 = geometry.add_point((c_x+r_x, c_y,0),lcar=0.5*res)
    p4 = geometry.add_point((c_x, c_y-r_y,0),lcar=0.5*res)
    arc_1 = geometry.add_ellipse_arc(p1, c, p2, p2)
    arc_2 = geometry.add_ellipse_arc(p2, c, p3, p3)
    arc_3 = geometry.add_ellipse_arc(p3, c, p4, p4)
    arc_4 = geometry.add_ellipse_arc(p4, c, p1, p1)
    obstacle_loop = geometry.add_line_loop([arc_1, arc_2, arc_3, arc_4])
    
    geometry.add_physical_line(obstacle_loop.lines, label=obstacle)
    # Outer boundary
    p1 = geometry.add_point((c_x-outer_r, c_y,0),lcar=res)    
    p2 = geometry.add_point((c_x, c_y+outer_r,0),lcar=res)
    p3 = geometry.add_point((c_x+outer_r, c_y,0),lcar=res)
    p4 = geometry.add_point((c_x, c_y-outer_r,0),lcar=res)
    arc_1 = geometry.add_ellipse_arc(p1, c, p2, p2)
    arc_2 = geometry.add_ellipse_arc(p2, c, p3, p3)
    arc_3 = geometry.add_ellipse_arc(p3, c, p4, p4)
    arc_4 = geometry.add_ellipse_arc(p4, c, p1, p1)
    outer_loop = geometry.add_line_loop([arc_1, arc_2, arc_3, arc_4])
    geometry.add_physical_line(outer_loop.lines, label=inflow)
    vol_in = geometry.add_plane_surface(outer_loop, holes=[obstacle_loop])
    vol0 = geometry.add_physical_surface(vol_in, 10)
    (points, cells, point_data,
     cell_data, field_data) = generate_mesh(geometry, prune_z_0=True,
                                            geo_filename="mesh_1.geo")
    meshio.write("mesh_1.xdmf", meshio.Mesh(
        points=points, cells={"triangle": cells["triangle"]}))

    meshio.write("mf_1.xdmf", meshio.Mesh(
        points=points, cells={"line": cells["line"]},
        cell_data={"line": {"name_to_read":
                            cell_data["line"]["gmsh:physical"]}}))

def goose_mesh(res=0.025, d=0.01):
    geo = Geometry()

    lines = []
    
    c = geo.add_point((0,0,0))
    p_bdy0 = geo.add_point((0,body_radiusy,0), 0.5*res)
    p_bdy1 = geo.add_point((body_radiusx,0,0), 0.5*res)
    p_bdy2 = geo.add_point((0,-body_radiusy, 0), 0.5*res)
    
    lines.append(-geo.add_ellipse_arc(p_bdy1, c, p_bdy2, p_bdy2))
    lines.append(-geo.add_ellipse_arc(p_bdy0, c, p_bdy1, p_bdy1))
    
    p_wng0 = geo.add_point((0.1*body_radiusx, body_radiusy+0.5*span_wing, 0),
                           0.5*res)
    p_wng1 = geo.add_point((0.5*body_radiusx, body_radiusy+span_wing, 0),
                           0.5*res)
    p_wng2 = geo.add_point((-width_wing, 0.8*span_wing+body_radiusy, 0),
                           0.5*res)
    p_wng3 = geo.add_point((-width_wing, body_radiusy, 0), 0.5*res)
    lines.append(geo.add_spline([p_bdy0,p_wng0, p_wng1, p_wng2, p_wng3]))

    p_bdy3 = geo.add_point((-body_radiusx-width_wing, neck_width/2,0), 0.5*res)
    lines.append(geo.add_ellipse_arc(p_wng3, c, p_bdy3, p_bdy3))
    
    p_neck0 = geo.add_point((-body_radiusx-width_wing-neck_length,neck_width/2,0), 0.5*res)
    lines.append(geo.add_line(p_bdy3, p_neck0))
    p_head0 = geo.add_point((-body_radiusx-width_wing-neck_length-1*head_radius,
                             neck_width/2+0.5*head_radius,0), 0.5*res)
    
    p_head1 = geo.add_point((-body_radiusx-width_wing-neck_length-2*head_radius,
                             neck_width/2,0), 0.5*res)
    lines.append(geo.add_spline([p_neck0, p_head0, p_head1]))
    p_head2 = geo.add_point((-body_radiusx-width_wing-neck_length-2*head_radius-beak_length,
                             0,0), 0.5*res)
    p_head3 = geo.add_point((-body_radiusx-width_wing-neck_length-2*head_radius,
                             -neck_width/2,0), 0.5*res)
    lines.append(geo.add_spline([p_head1, p_head2, p_head3]))
    
    p_head4 = geo.add_point((-body_radiusx-width_wing-neck_length-1*head_radius,
                             -neck_width/2-0.5*head_radius,0), 0.5*res)

    p_neck1 = geo.add_point((-body_radiusx-width_wing-neck_length,-neck_width/2,0), 0.5*res)
    lines.append(geo.add_spline([p_head3, p_head4, p_neck1]))

    p_bdy4 = geo.add_point((-body_radiusx-width_wing, -neck_width/2,0), 0.5*res)
    lines.append(geo.add_line(p_neck1,p_bdy4))

    p_wng4 = geo.add_point((-width_wing, -body_radiusy, 0), 0.5*res)
    lines.append(-geo.add_ellipse_arc(p_wng4, c, p_bdy4, p_bdy4))

    p_wng5 = geo.add_point((0.1*body_radiusx, -(body_radiusy+0.5*span_wing), 0), 0.5*res)
    p_wng6 = geo.add_point((0.5*body_radiusx, -(body_radiusy+span_wing), 0), 0.5*res)
    p_wng7 = geo.add_point((-width_wing, -(0.8*span_wing+body_radiusy), 0), 0.5*res)
    lines.append(geo.add_spline([p_wng4, p_wng7, p_wng6, p_wng5,p_bdy2]))
    obstacle_loop = geo.add_line_loop(lines)

    # outer_circle
    surface = geo.add_circle((0,0,0), outer_r, holes=[obstacle_loop], lcar=res)

    geo.add_physical_line(obstacle_loop.lines, label=obstacle)
    vol0 = geo.add_physical_surface(surface.plane_surface, 10)
    (points, cells, point_data,
     cell_data, field_data) = generate_mesh(geo, prune_z_0=True,
                                            geo_filename="goose.geo")
    meshio.write("mesh_1.xdmf", meshio.Mesh(
        points=points, cells={"triangle": cells["triangle"]}))

    meshio.write("mf_1.xdmf", meshio.Mesh(
        points=points, cells={"line": cells["line"]},
        cell_data={"line": {"name_to_read":
                            cell_data["line"]["gmsh:physical"]}}))

    
if __name__=="__main__":
    import sys
    res = 0.023
    # Goose
    # res = 0.013
    obstacle_mesh(res)
    channel_mesh(res)
    refined_mesh(res)
    #goose_mesh(res)
