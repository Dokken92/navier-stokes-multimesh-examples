# Navier-Stokes with the multimesh finite element method.

This repository consists of a collection of material for the Navier-Stokes equation, solved with the multimesh finite element method.

## Installation

To get a dolfin image, using the MultiMesh uflacs implementation, there is a docker image available. To download it, run
`docker pull dokken92/multimesh-uflacs`. The dockerfiles are available at `https://github.com/jorgensd/multimesh-uflacs-docker/blob/master/Dockerfile`.
It is also possible to use the quay images supplied by fenics-project in docker images.
However, note that there are some bugs in the quadrature implementations, as `jump(grad(MultiMeshFunction(V)))` on an interface is not well defined.

The multimesh solvers used in this repository can be installed in the docker image by calling
`pip install .`
in the source directory.