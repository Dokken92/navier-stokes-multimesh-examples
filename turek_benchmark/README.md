Usage
------
To run simulations and postprocessing for coarse meshes, where we have a total of 15 114 active dofs, with a Crank-Nicoloson and BDF2 scheme, run `make coarse`.

For a simulation with For a simulation with 32 271 active dofs, run `make fine`. To change parameters in the simulation run `python3 bench.py --help`.  dofs, run `make fine`. To change parameters in the simulation run `python3 bench.py --help`.


Figures
-------
**Comparasion of MM-FEM for BDF2 and CN with ~15000 cells [Turek Benchmark](http://www.featflow.de/en/benchmarks/cfdbenchmarking/flow/dfg_benchmark3_re100.html) with ~40 000 dofs**

![](drag_lift_15114.png)

**
Comparasion of MM-FEm with 32 271 dofs vs Turek Benchmark ~40 000 dofs
**

![](drag_lift_32271.png)