import numpy as np
import matplotlib.pyplot as plt
from mesh_analysis import analyse_mesh
from create_meshes import channel_mesh, obstacle_mesh

def visualize_bench(CN, lvl=4, res=0.015):
    channel_mesh(res)
    obstacle_mesh(res)
    N = analyse_mesh()
    infile = np.load("results_BDF2/{0:d}/results.npz".format(N))
    if CN:
        infile_CN = np.load("results_CN/{0:d}/results.npz".format(N))

    turek = np.loadtxt("bdforces_lv{0:d}".format(lvl))
    turek_p = np.loadtxt("pointvalues_lv{0:d}".format(lvl))
    
    fig = plt.figure()
    plt.subplot(3,2,1)
    l1 = plt.plot(infile["t"][1:],infile["CD"][1:], label=r"MM BDF2")
    if CN:
        linestyle = "--"
        l2 = plt.plot(infile_CN["t"][1:],infile_CN["CD"][1:], label=r"MM CN", linestyle=linestyle)
    l5 = plt.plot(turek[1:,1], turek[1:,3], marker="x", markevery=100, 
                  linestyle="", markersize=1, label="Turek Lv {0:d}".format(lvl))

    plt.ylabel(r"$C_D$")
    plt.axis([0,8,-0.5,3])
    lines = [l1[0],l5[0]]
    if CN:
        lines = lines + [l2[0]]
    leg = plt.legend( lines, [l.get_label() for l in lines],
                      loc = 'center right', bbox_to_anchor = (1,0.5),
                      bbox_transform = plt.gcf().transFigure )
    leg.get_frame().set_linewidth(0.0)
    plt.subplots_adjust(right=0.8)
    
    plt.grid()

    plt.subplot(3,2,2)
    plt.grid()
    plt.subplots_adjust(wspace=0.4)
    plt.ylabel("Abs Error")
    if len(infile["t"][1:]) == len(turek[1:,3]):

        l11 = plt.plot(infile["t"][1:],abs(infile["CD"][1:]-turek[1:,3]), label=r"MM BDF2")
    else:
        y_data = np.interp(turek[1:,1],infile["t"][1:], infile["CD"][1:])
        plt.plot(turek[1:,1], abs(y_data-turek[1:,3]), label=r"MM BDF2")
    if CN:
        l21 = plt.plot(infile_CN["t"][1:],abs(infile_CN["CD"][1:]-turek[1:,3]), label=r"MM CN", linestyle=linestyle)
    plt.yscale('log')
    # if CN:
    #     linestyle = "--"
    #     l2 = plt.plot(infile_CN["t"][1:],infile_CN["CD"][1:], label=r"MM CN", linestyle=linestyle)
    # l5 = plt.plot(turek[1:,1], turek[1:,3], marker="x", markevery=100, 
    #               linestyle="", markersize=1, label="Turek Lv {0:d}".format(lvl))2
    
    plt.subplot(3,2,3)
    plt.ylabel(r"$C_L$")
    plt.plot(infile["t"],infile["CL"], label=r"MultiMesh BDF2")
    if CN:
        plt.plot(infile_CN["t"],infile_CN["CL"], label=r"MultiMesh CN", linestyle=linestyle)
    plt.plot(turek[1:,1], turek[1:,4],marker="X", markevery=100,
             linestyle="", markersize=1,
             label="Turek lvl {0:d}".format(lvl))
    plt.axis([0,8,-0.5,0.5])
    #plt.legend()
    plt.grid()

    plt.subplot(3,2,4)
    plt.grid()
    plt.subplots_adjust(wspace=0.4)
    plt.ylabel("Abs Error")
    if len(infile["t"][1:]) == len(turek[1:,3]):
        l11 = plt.plot(infile["t"][1:],abs(infile["CL"][1:]-turek[1:,4]), label=r"MM BDF2")
    else:
        y_data = np.interp(turek[1:,1],infile["t"][1:], infile["CL"][1:])
        plt.plot(turek[1:,1], abs(y_data-turek[1:,4]), label=r"MM BDF2")
    if CN:
        l21 = plt.plot(infile_CN["t"][1:],abs(infile_CN["CL"][1:]-turek[1:,4]), label=r"MM CN", linestyle=linestyle)
    plt.yscale('log')

    
    plt.subplot(3,2,5)
    plt.ylabel(r"$\Delta p$")
    plt.plot(infile["t"], infile["dp"], label=r"MM BDF2")
    if CN:
        plt.plot(infile_CN["t"], infile_CN["dp"], label=r"MM CN", linestyle=linestyle)
    plt.plot(turek_p[1:,1], turek_p[1:,6]-turek_p[1:,-1],marker="X", markevery=100,
             linestyle="", markersize=1,label="Turek lvl {0:d}".format(lvl))
    plt.grid()
    plt.axis([0,8,-0.5, 2.5])
    plt.xlabel("t")
    #plt.legend()

    plt.subplot(3,2,6)
    plt.grid()
    plt.subplots_adjust(wspace=0.4)
    plt.ylabel("Abs Error")
    if len(infile["t"][1:]) == len(turek[1:,3]):
        l11 = plt.plot(infile["t"][1:],abs(infile["dp"][1:]-(turek_p[1:,6]-turek_p[1:,-1])), label=r"MM BDF2")
    else:
        y_data = np.interp(turek[1:,1],infile["t"][1:], infile["dp"][1:])
        plt.plot(turek[1:,1], abs(y_data-(turek_p[1:,6]-turek_p[1:,-1])), label=r"MM BDF2")

    if CN:
        l21 = plt.plot(infile_CN["t"][1:],abs(infile_CN["dp"][1:]-(turek_p[1:,6]-turek_p[1:,-1])), label=r"MM CN", linestyle=linestyle)
    plt.yscale('log')

    plt.savefig("drag_lift_{0:d}.png".format(N), dpi=500)

    # n_L = np.argmax(infile["CL"])
    # t_l = infile["t"][n_L]
    # n_D = np.argmax(infile["CD"])
    # t_d = infile["t"][n_D]
    # print("Max lift ", t_l, infile["CL"][n_L])
    # print("Max drag ", t_d, infile["CD"][n_D])
    # print("dp(T=8) ", infile["dp"][-1])

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-res", type=float, default=0.015)
    feature_parser2 = parser.add_mutually_exclusive_group(required=False)
    feature_parser2.add_argument('--CN', dest='CN',
                                action='store_true')
    feature_parser2.add_argument('--no-CN', dest='CN',
                                 action='store_false')
    parser.set_defaults(CN=False)
    args = parser.parse_args()
    import sys
    thismodule = sys.modules[__name__]
    for key in vars(args):
        setattr(thismodule, key, getattr(args, key))
    
    visualize_bench(CN, res=res)
