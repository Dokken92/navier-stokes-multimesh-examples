from dolfin import *
from dolfin.cpp.mesh import MeshFunctionSizet
from create_meshes import inflow, outflow, walls, obstacle, c_x, c_y, channel_mesh, obstacle_mesh
from mesh_analysis import analyse_mesh

import numpy as np
from IPython import embed
set_log_level(LogLevel.ERROR)

class BoundaryFunction(UserExpression):
    def __init__(self, t, **kwargs):
        super().__init__(**kwargs)
        self.t = t

    def eval(self, values, x):
        U = 1.5*sin(pi*self.t/8)
        values[0] = 4*U*x[1]*(0.41-x[1])/pow(0.41, 2)
        values[1] = 0
    
    def value_shape(self):
        return (2,)

def benchmark(nu, T, dt, degrees, alpha,
               beta, results_dir="results/", BDF2=True):
    (degree_u, degree_p) = degrees
    f = Constant((0,0))

    # Load background mesh
    mesh_0 = Mesh()
    with XDMFFile("mesh_0.xdmf") as infile:
        infile.read(mesh_0)
    mvc = MeshValueCollection("size_t", mesh_0, 1)
    with XDMFFile("mf_0.xdmf") as infile:
        infile.read(mvc, "name_to_read")
    mf_0 = MeshFunctionSizet(mesh_0, mvc)

    # Load object mesh
    mesh_1 = Mesh()
    with XDMFFile("mesh_1.xdmf") as infile:
        infile.read(mesh_1)
    mvc = MeshValueCollection("size_t", mesh_1, 1)
    with XDMFFile("mf_1.xdmf") as infile:
        infile.read(mvc, "name_to_read")
    mf_1 = MeshFunctionSizet(mesh_1, mvc)

    # Build MultiMeshes
    multimesh = MultiMesh()
    for mesh in [mesh_0, mesh_1]:
        multimesh.add(mesh)
    multimesh.build(2*degree_u)
    multimesh.auto_cover(0, Point(c_x,c_y))

    num_parts = multimesh.num_parts()

    # Build function spaces 
    V_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u)
    V = MultiMeshFunctionSpace(multimesh, V_el)

    Q_el = FiniteElement("CG", mesh_0.ufl_cell(), degree_p)
    Q = MultiMeshFunctionSpace(multimesh, Q_el)

    # Set-up functions
    u_h = MultiMeshFunction(V)
    u_old = MultiMeshFunction(V)
    u_s = MultiMeshFunction(V)

    p_h = MultiMeshFunction(Q)
    # For CN load solution at staggered timestep
    # if not BDF2:
    #     from init_CN import initialize_CN
    #    initialize_CN(nu, dt, degrees, alpha, beta, results_dir="init_CN/")
    #     ps = [p_h.part(i,deepcopy=True) for i in range(num_parts)]
    #     init_p = [XDMFFile("init_CN/p_part_{0:d}.xdmf".format(i)) for i in range(num_parts)]
    #     for i in range(num_parts):
    #         init_p[i].read_checkpoint(ps[i],"p", 0)
    #         p_h.assign_part(i, ps[i])
    #     [ip.close() for ip in init_p]

    p_c = MultiMeshFunction(Q)

    dt_c = Constant(dt)

    # Setup for step 1: computing tentative velocity
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # N.B. Full explicit, 1st order convection add_terms
    n = FacetNormal(multimesh)
    h = 2.0*Circumradius(multimesh)

    tensor_jump = lambda v, n: outer(v('+'), n('+')) + outer(v('-'), n('-'))

    weight_time = Constant(3/(2*dt_c)) if BDF2 else Constant(1/dt_c)
    weight_diffusion = nu if BDF2 else Constant(0.5)*nu
    a1 = (weight_time*inner(u, v) + weight_diffusion*inner(grad(u),grad(v)))*dX
    L1 = (inner(p_h, div(v)) + inner(f, v))*dX
    L1 -= avg(p_h)*inner(n("-"), v("-")-v("+"))*dI
    L1 -= jump(p_h)*inner(n("-"), avg(v))*dI
    if BDF2:
        L1 += (1/(2*dt_c)*inner(4*u_h - u_old, v)
              - 2*inner(grad(u_h)*u_h, v)
              + inner(grad(u_old)*u_old, v))*dX
    else:
        u_AB = Constant(1.5)*u_h-Constant(0.5)*u_old
        a1 += Constant(0.5)*inner(grad(u)*u_AB, v)*dX
        # RHS
        L1 += (weight_time*inner(u_h, v)
              - weight_diffusion*inner(grad(u_h), grad(v))
              - 0.5*inner(grad(u_h)*u_AB, v))*dX
        # - inner(3/2*grad(u_h)*u_h-0.5*grad(u_old)*u_old, v))*dX

        L1 += weight_diffusion*(inner(dot(grad(u_h("-")), n("-")), v("-"))-
                                inner(dot(grad(u_h("+")), n("-")), v("+")))*dI
    # Nitsche terms
    a1 += weight_diffusion*(-inner(avg(grad(u)), tensor_jump(v, n))*dI
              -inner(avg(grad(v)), tensor_jump(u, n))*dI
              +alpha/avg(h)*inner(jump(u), jump(v))*dI)
    a1 += weight_diffusion*beta*inner(jump(grad(u)), jump(grad(v)))*dO
    # Stabilization of mass terms
    a1 += weight_time*beta*inner(jump(u), jump(v))*dO


    # Define boundary conditions
    t = 0
    U_inlet = BoundaryFunction(t)
    bc_inlet = MultiMeshDirichletBC(V, U_inlet, mf_0, inflow, 0)
    bc_walls = MultiMeshDirichletBC(V, Constant((0,0)), mf_0, walls, 0)
    bc_obstacle = MultiMeshDirichletBC(V, Constant((0,0)), mf_1, obstacle, 1) 
    bcs_u = [bc_inlet, bc_walls, bc_obstacle]

    # Assemble left hand sides and apply boundary conditions
    if BDF2:
        A1 = assemble_multimesh(a1)
    #[bc_u.apply(A1) for bc_u in bcs_u]
    #bc_inlet.apply(u_h.vector())
    #bc_inlet.apply(u_old.vector())
    # Assemble L just to apply lock_inactive_dofs for A
    # lock_inactive_dofs should have additional variants
    # for only A or b
    b1 = assemble_multimesh(L1)
    
    # Setup for step 2: compute pressure update
    p = TrialFunction(Q)
    q = TestFunction(Q)
    
    a2 = inner(grad(p), grad(q))*dX

    # Nitsche terms 
    a2 += (-dot(avg(grad(p)), jump(q, n))
           -dot(avg(grad(q)), jump(p, n))
           +alpha/avg(h)*inner(jump(p), jump(q)))*dI
    
    
    # Overlap stabilization for p
    a2 += beta*inner(jump(grad(p)), jump(grad(q)))*dO
    L2 = -weight_time*inner(div(u_s), q)*dX

    A2 = assemble_multimesh(a2)
    b2 = assemble_multimesh(L2)
    bc_p = MultiMeshDirichletBC(Q, Constant(0), mf_0, outflow, 0)
    
    # create solver and factorize matrix
    solver_2 = LUSolver(A2, "mumps")

    # Setup for step 3: update velocity
    a3 = inner(u, v)*dX
    # Overlap stabilization 
    # also cover dI related stabilizations
    a3 += beta*inner(jump(u), jump(v))*dO
    L3 = (inner(u_s, v) - 1/weight_time*inner(grad(p_c), v))*dX
    A3 = assemble_multimesh(a3)
    b3 = assemble_multimesh(L3)
    V.lock_inactive_dofs(A3, b3)
    
    # create solver and factorize matrix
    solver_3 = LUSolver(A3, "mumps")

    
    # Define output files and write out data for initial step
    xdmffile_u = [XDMFFile(results_dir + "u_part_{0:d}.xdmf".format(i))
                  for i in range(num_parts)]
    xdmffile_p = [XDMFFile(results_dir + "p_part_{0:d}.xdmf".format(i))
                  for i in range(num_parts)]

    for i in range(num_parts):
        xdmffile_u[i].write(u_h.part(i), t)
        t_p = t if BDF2 else t + dt/2
        xdmffile_p[i].write(p_h.part(i), t_p)

    # Compute reference quantities
    n1 = -FacetNormal(mesh_1) #Normal pointing out of obstacle
    dObs = Measure("ds", domain=mesh_1,
                   subdomain_data=mf_1, subdomain_id=obstacle)
    p1 = p_h.part(1, deepcopy=True)
    u1 = u_h.part(1, deepcopy=True)
    u_t = inner(as_vector((n1[1], -n1[0])), u1)
    drag = assemble(2/0.1*(nu*inner(grad(u_t), n1)*n1[1] - p1*n1[0])*dObs)
    lift = assemble(-2/0.1*(nu*inner(grad(u_t), n1)*n1[0] + p1*n1[1])*dObs)
    p_diffs = [p1(0.15,0.2)-p1(0.25,0.2)]
    c_ds = [drag]
    c_ls = [lift]
    ts = [t]

    # Solve problem
    D = T/(100*dt)
    sample_time = [i*100 for i in range(1,int(D+1))] + [j for j in range(1,11)]
    c = 0
    while(t <= T-dt*10e-3):
        c += 1
        #print(".", end="")
        if c in sample_time:
            print("Time {0:.2e}, delta p={1:.2e}, Lift: {2:.2e}, Drag: {3:.2e}"
                  .format(t, p_diffs[-1], c_ls[-1], c_ds[-1]))
        # Increase time and update data
        t += dt
        U_inlet.t = t

        # Step 1: compute tentative velocity 
        b1 = assemble_multimesh(L1)
        if not BDF2:
            A1 = assemble_multimesh(a1)
        [bc_u.apply(A1,b1) for bc_u in bcs_u]
        V.lock_inactive_dofs(A1, b1)
        solver_1 = LUSolver(A1, "mumps")
        solver_1.solve(u_s.vector(), b1)
        
        # Step 2: compute pressure correction
        b2 = assemble_multimesh(L2)
        Q.lock_inactive_dofs(A2, b2)
        bc_p.apply(A2, b2)
        solver_2.solve(p_c.vector(), b2)

        # Update pressure
        p_h.vector().axpy(1.0, p_c.vector())
        if c in sample_time:
            for i in range(num_parts):
                t_p = t if BDF2 else t + dt/2
                xdmffile_p[i].write(p_h.part(i), t_p)
    
        # Assign new u_old
        u_old.assign(u_h)

        # Step 3: compute velocity update
        b3 = assemble_multimesh(L3)
        V.lock_inactive_dofs(A3, b3)
        solver_3.solve(u_h.vector(), b3)
        # Write out computed u
        if c in sample_time:
            for i in range(num_parts):
                xdmffile_u[i].write(u_h.part(i), t)

        # Compute reference quantities
        p1 = p_h.part(1, deepcopy=True)
        u1 = u_h.part(1, deepcopy=True)
        u_t = inner(as_vector((n1[1], -n1[0])), u1)
        c_ds.append(assemble(
            2/0.1*(nu*inner(grad(u_t), n1)*n1[1] - p1*n1[0])*dObs))
        c_ls.append(assemble(
            -2/0.1*(nu*inner(grad(u_t), n1)*n1[0] + p1*n1[1])*dObs))
        p_diffs.append(p1(0.15,0.2)-p1(0.25,0.2))
        ts.append(t)
        
    [out.close() for out in xdmffile_u]
    [out.close() for out in xdmffile_p]
    np.savez(results_dir+"results", dp=np.array(p_diffs),
             CD=np.array(c_ds), CL=np.array(c_ls),
             t=ts)

    print("Simulation ended")
    

if __name__ == "__main__":
    import time
    start = time.time() 

    degrees = (2,1)
    alpha, beta = 50, 10

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-T", type=float, const=1, nargs="?",
                        help="Endtime of simulation", default=8)
    parser.add_argument("-nu", type=float, const=1, nargs="?",
                        help="Kinematic viscosity", default=0.001)
    parser.add_argument("-dt", type=float, const=1, nargs="?",
                        help="Time step", default=1/1600)
    parser.add_argument("-res", help="Mesh resolution at inflow",
                        type=float, default=0.015)
    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--BDF2', dest='BDF2',
                                action='store_true')
    feature_parser.add_argument('--CN', dest='BDF2',
                                action='store_false')
    parser.set_defaults(BDF2=True)
    args = parser.parse_args()
    import sys
    thismodule = sys.modules[__name__]
    for key in vars(args):
        setattr(thismodule, key, getattr(args, key))
    print("Generating mesh")
    channel_mesh(res)
    obstacle_mesh(res)
    n_dofs = analyse_mesh()
    print("Total dofs {0:d}".format(int(n_dofs)))
    results_dir = "results_BDF2/" if BDF2 else "results_CN/"
    results_dir += "{0:d}/".format(int(n_dofs))
    args = (nu,T, dt, degrees, alpha, beta)
    benchmark(*args, BDF2=BDF2, results_dir=results_dir)

    end = time.time()
    print("Runtime: {0:.2e}".format(end-start))
