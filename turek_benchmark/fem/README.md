Usage
------
To run simulations and postprocessing for coarse meshes, where we have a total of 15 030 dofs, with a Crank-Nicoloson and BDF2 scheme, run `make coarse`.

For a simulation with 32 716 dofs, run `make fine`. To change parameters in the simulation run `python3 bench.py --help`.
