
def analyse_mesh():
    from dolfin import (Mesh, XDMFFile, VectorElement, FiniteElement,
                        FunctionSpace)
    mesh = Mesh()
    with XDMFFile("mesh.xdmf") as infile:
        infile.read(mesh)    
    degree_u = 2
    degree_p = 1
    V_el = VectorElement("CG", mesh.ufl_cell(), degree_u)
    V = FunctionSpace(mesh, V_el)
    
    Q_el = FiniteElement("CG", mesh.ufl_cell(), degree_p)
    Q = FunctionSpace(mesh, Q_el)

    print("total dofs V", V.dim())
    print("total dofs Q", Q.dim())
    return(V.dim()+Q.dim())

if __name__ == "__main__":
    tot_dof = analyse_mesh()
    print(tot_dof)
