from dolfin import *
from dolfin.cpp.mesh import MeshFunctionSizet
from create_mesh import inflow, outflow, walls, obstacle, c_x, c_y, channel_mesh
from mesh_analysis import analyse_mesh

import numpy as np
from IPython import embed
set_log_level(LogLevel.ERROR)

class BoundaryFunction(UserExpression):
    def __init__(self, t, **kwargs):
        super().__init__(**kwargs)
        self.t = t

    def eval(self, values, x):
        U = 1.5*sin(pi*self.t/8)
        values[0] = 4*U*x[1]*(0.41-x[1])/pow(0.41, 2)
        values[1] = 0
    
    def value_shape(self):
        return (2,)

def benchmark(nu, T, dt, degrees,
              results_dir="results/", BDF2=True):
    (degree_u, degree_p) = degrees
    f = Constant((0,0))

    # Load background mesh
    mesh = Mesh()
    with XDMFFile("mesh.xdmf") as infile:
        infile.read(mesh)
    mvc = MeshValueCollection("size_t", mesh, 1)
    with XDMFFile("mf.xdmf") as infile:
        infile.read(mvc, "name_to_read")
    mf = MeshFunctionSizet(mesh, mvc)

    # Build function spaces 
    V_el = VectorElement("CG", mesh.ufl_cell(), degree_u)
    V = FunctionSpace(mesh, V_el)

    Q_el = FiniteElement("CG", mesh.ufl_cell(), degree_p)
    Q = FunctionSpace(mesh, Q_el)

    # Set-up functions
    u_h = Function(V)
    u_old = Function(V)
    u_s = Function(V)

    p_h = Function(Q)
    # if not BDF2:
    #     from init_CN import init_pressure
    #     init_pressure(nu, dt, degrees, results_dir="init_CN/")
    #     XDMF_init = XDMFFile("init_CN/p.xdmf")
    #     XDMF_init.read_checkpoint(p_h, "p", 0)
    
    p_c = Function(Q)

    dt_c = Constant(dt)
    n = FacetNormal(mesh)
    # Setup for step 1: computing tentative velocity
    u = TrialFunction(V)
    v = TestFunction(V)
    weight_time = Constant(3/(2*dt_c)) if BDF2 else Constant(1/dt_c)
    weight_diffusion = nu if BDF2 else Constant(0.5)*nu
    a1 = (weight_time*inner(u, v) + weight_diffusion*inner(grad(u),grad(v)))*dX
    L1 = inner(f, v)*dx\
         + p_h*div(v)*dx
         #-inner(grad(p_h), v)*dx + - weight_diffusion*p_h*inner(n, v)*ds(subdomain_data=mf,
         #                                                                subdomain_id=outflow)
    
    if BDF2:
        L1 += (1/(2*dt_c)*inner(4*u_h - u_old, v)
              - 2*inner(grad(u_h)*u_h, v)
              + inner(grad(u_old)*u_old, v))*dx
    else:
        u_AB = Constant(1.5)*u_h-Constant(0.5)*u_old
        a1 += Constant(0.5)*inner(grad(u)*u_AB, v)*dx
        # RHS
        L1 += (weight_time*inner(u_h, v)
               - weight_diffusion*inner(grad(u_h), grad(v))
               - Constant(0.5)*inner(grad(u_h)*u_AB, v))*dx
               # - inner(3/2*grad(u_h)*u_h-0.5*grad(u_old)*u_old, v))*dx
               

    # Define boundary conditions
    t = 0
    U_inlet = BoundaryFunction(t)
    bc_inlet = DirichletBC(V, U_inlet, mf, inflow)
    bc_walls = DirichletBC(V, Constant((0,0)), mf, walls)
    bc_obstacle = DirichletBC(V, Constant((0,0)), mf, obstacle) 
    bcs_u = [bc_inlet, bc_walls, bc_obstacle]

    # Assemble left hand sides and apply boundary conditions

    #bc_inlet.apply(u_h.vector())
    #bc_inlet.apply(u_old.vector())
    # Assemble L just to apply lock_inactive_dofs for A
    # lock_inactive_dofs should have additional variants
    # for only A or b
    b1 = assemble(L1)
    [bc_u.apply(b1) for bc_u in bcs_u]
    
    # create solver and factorize matrix
    if BDF2:
        A1 = assemble(a1)

    # Setup for step 2: compute pressure update
    p = TrialFunction(Q)
    q = TestFunction(Q)
    
    a2 = inner(grad(p), grad(q))*dx
    L2 = -weight_time*inner(div(u_s), q)*dx
    A2 = assemble(a2)
    b2 = assemble(L2)
    bc_p = DirichletBC(Q, Constant(0), mf, outflow)
    
    # create solver and factorize matrix
    solver_2 = LUSolver(A2, "mumps")

    # Setup for step 3: update velocity
    a3 = inner(u, v)*dx
    L3 = (inner(u_s, v) - 1/weight_time*inner(grad(p_c), v))*dx

    A3 = assemble(a3)
    b3 = assemble(L3)
    
    # create solver and factorize matrix
    solver_3 = LUSolver(A3, "mumps")

    
    # Define output files and write out data for initial step
    xdmffile_u = File(results_dir + "u.pvd")
    xdmffile_p = File(results_dir + "p.pvd")

    xdmffile_u.write(u_h, t)
    xdmffile_p.write(p_h, t)

    # Compute reference quantities
    n1 = -FacetNormal(mesh) #Normal pointing out of obstacle
    dObs = Measure("ds", domain=mesh,
                   subdomain_data=mf, subdomain_id=obstacle)
    u_t = inner(as_vector((n1[1], -n1[0])), u_h)
    drag = assemble(2/0.1*(nu*inner(grad(u_t), n1)*n1[1] - p_h*n1[0])*dObs)
    lift = assemble(-2/0.1*(nu*inner(grad(u_t), n1)*n1[0] + p_h*n1[1])*dObs)
    p_diffs = [p_h(0.15,0.2)-p_h(0.25,0.2)]
    c_ds = [drag]
    c_ls = [lift]
    ts = [t]

    # Solve problem
    D = int(T/(100*dt))
    sample_time = [i*100 for i in range(1,D+1)] + [j for j in range(1,11)]
    c = 0
    while(t <= T-dt*10e-3):
        c += 1
        #print(".", end="")
        if c in sample_time:
            print("Time {0:.2e}, delta p={1:.2e}, Lift: {2:.2e}, Drag: {3:.2e}"
                  .format(t, p_diffs[-1], c_ls[-1], c_ds[-1]))
        # Increase time and update data
        t += dt
        U_inlet.t = t

        # Step 1: compute tentative velocity 
        b1 = assemble(L1)
        if not BDF2:
            A1 = assemble(a1)
        [bc_u.apply(A1,b1) for bc_u in bcs_u]
        solver_1 = LUSolver(A1, "mumps")
        solver_1.solve(u_s.vector(), b1)
 
        # Step 2: compute pressure correction
        b2 = assemble(L2)
        bc_p.apply(A2, b2)
        solver_2.solve(p_c.vector(), b2)

        # Update pressure
        p_h.vector().axpy(1.0, p_c.vector())
        if c in sample_time:
            xdmffile_p.write(p_h, t)

        # Assign new u_old
        u_old.assign(u_h)

        # Step 3: compute velocity update
        b3 = assemble(L3)
        solver_3.solve(u_h.vector(), b3)
        if c in sample_time:
            xdmffile_u.write(u_h, t)

        # Compute reference quantities
        c_ds.append(assemble(
            2/0.1*(nu*inner(grad(u_t), n1)*n1[1] - p_h*n1[0])*dObs))
        c_ls.append(assemble(
            -2/0.1*(nu*inner(grad(u_t), n1)*n1[0] + p_h*n1[1])*dObs))
        p_diffs.append(p_h(0.15,0.2)-p_h(0.25,0.2))
        ts.append(t)
        
    #xdmffile_u.close()
    #xdmffile_p.close()
    np.savez(results_dir+"results", dp=np.array(p_diffs),
             CD=np.array(c_ds), CL=np.array(c_ls),
             t=ts)

    print("Simulation ended")
    

if __name__ == "__main__":
    import time
    start = time.time() 

    degrees = (2,1)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-T", type=float, const=1, nargs="?",
                        help="Endtime of simulation", default=8)
    parser.add_argument("-nu", type=float, const=1, nargs="?",
                        help="Kinematic viscosity", default=0.001)
    parser.add_argument("-dt", type=float, const=1, nargs="?",
                        help="Time step", default=1/1600)
    parser.add_argument("-res", help="Mesh resolution at inflow",
                        type=float, default=0.015)
    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--BDF2', dest='BDF2',
                                action='store_true')
    feature_parser.add_argument('--CN', dest='BDF2',
                                action='store_false')
    parser.set_defaults(BDF2=True)
    args = parser.parse_args()
    import sys
    thismodule = sys.modules[__name__]
    for key in vars(args):
        setattr(thismodule, key, getattr(args, key))
    print("Generating mesh")
    channel_mesh(res)
    n_dofs = analyse_mesh()
    print("Total dofs {0:d}".format(int(n_dofs)))
    results_dir = "results_BDF2/" if BDF2 else "results_CN/"
    results_dir += "{0:d}/".format(int(n_dofs))
    args = (nu,T, dt, degrees)
    benchmark(*args, BDF2=BDF2, results_dir=results_dir)

    end = time.time()
    print("Runtime: {0:.2e}".format(end-start))
