from pygmsh import generate_mesh
from pygmsh.built_in.geometry import Geometry
import meshio

inflow = 1
outflow = 2
walls = 3
obstacle = 4

L = 2.2
H = 0.41
c_x,c_y = 0.2,0.2
r_x = 0.05
d = 0.1
r_o =r_x+d

def channel_mesh(res=0.025):
    """ 
    Creates a unstructured rectangular mesh
    """
    geometry = Geometry()
    points = [geometry.add_point((0, 0, 0),lcar=res),
              geometry.add_point((L, 0, 0),lcar=5*res),
              geometry.add_point((L, H, 0),lcar=5*res),
              geometry.add_point((0, H, 0),lcar=res)]
    lines = [geometry.add_line(points[i], points[i+1])
             for i in range(len(points)-1)]
    lines.append(geometry.add_line(points[-1], points[0]))
    ll = geometry.add_line_loop(lines)
    s = geometry.add_plane_surface(ll)
    #rectangle = geometry.add_rectangle(0,L,0,H,0, res)
    outflow_list = [lines[1]]
    flow_list = [lines[-1]]
    wall_list = [lines[0], lines[2]]

    geometry.add_physical_surface(s,label=12)
    geometry.add_physical_line(flow_list, label=inflow)
    geometry.add_physical_line(outflow_list, label=outflow)
    geometry.add_physical_line(wall_list, label=walls)
    

    (points, cells, point_data,
     cell_data, field_data) = generate_mesh(geometry, prune_z_0=True,
                                            geo_filename="mesh_0.geo")
    
    meshio.write("mesh_0.xdmf", meshio.Mesh(
        points=points, cells={"triangle": cells["triangle"]}))
    
    meshio.write("mf_0.xdmf", meshio.Mesh(
        points=points, cells={"line": cells["line"]},
        cell_data={"line": {"name_to_read":
                            cell_data["line"]["gmsh:physical"]}}))

def obstacle_mesh(res=0.025, d=0.01):
    geometry = Geometry()
    c = geometry.add_point((c_x,c_y,0))    

    # Elliptic obstacle
    p1 = geometry.add_point((c_x-r_x, c_y,0),lcar=0.5*res)
    p2 = geometry.add_point((c_x, c_y+r_x,0),lcar=0.5*res)
    p3 = geometry.add_point((c_x+r_x, c_y,0),lcar=0.5*res)
    p4 = geometry.add_point((c_x, c_y-r_x,0),lcar=0.5*res)
    arc_1 = geometry.add_ellipse_arc(p1, c, p2, p2)
    arc_2 = geometry.add_ellipse_arc(p2, c, p3, p3)
    arc_3 = geometry.add_ellipse_arc(p3, c, p4, p4)
    arc_4 = geometry.add_ellipse_arc(p4, c, p1, p1)
    obstacle_loop = geometry.add_line_loop([arc_1, arc_2, arc_3, arc_4])
    
    geometry.add_physical_line(obstacle_loop.lines, label=obstacle)
    # Outer boundary
    p1 = geometry.add_point((c_x-r_o, c_y,0),lcar=res)    
    p2 = geometry.add_point((c_x, c_y+r_o,0),lcar=res)
    p3 = geometry.add_point((c_x+r_o, c_y,0),lcar=res)
    p4 = geometry.add_point((c_x, c_y-r_o,0),lcar=res)
    arc_1 = geometry.add_ellipse_arc(p1, c, p2, p2)
    arc_2 = geometry.add_ellipse_arc(p2, c, p3, p3)
    arc_3 = geometry.add_ellipse_arc(p3, c, p4, p4)
    arc_4 = geometry.add_ellipse_arc(p4, c, p1, p1)
    outer_loop = geometry.add_line_loop([arc_1, arc_2, arc_3, arc_4])
    geometry.add_physical_line(outer_loop.lines, label=inflow)
    vol_in = geometry.add_plane_surface(outer_loop, holes=[obstacle_loop])
    vol0 = geometry.add_physical_surface(vol_in, 10)
    (points, cells, point_data,
     cell_data, field_data) = generate_mesh(geometry, prune_z_0=True,
                                            geo_filename="mesh_1.geo")
    meshio.write("mesh_1.xdmf", meshio.Mesh(
        points=points, cells={"triangle": cells["triangle"]}))

    meshio.write("mf_1.xdmf", meshio.Mesh(
        points=points, cells={"line": cells["line"]},
        cell_data={"line": {"name_to_read":
                            cell_data["line"]["gmsh:physical"]}}))
        
if __name__=="__main__":

    obstacle_mesh(0.01)
    channel_mesh(0.01)

    # obstacle_mesh(0.005)
    # channel_mesh(0.005)
