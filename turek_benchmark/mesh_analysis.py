
def analyse_mesh():
    from dolfin import (Mesh, XDMFFile, VectorElement, FiniteElement,
                        MultiMeshFunctionSpace, MultiMesh, Point)
    # Build MultiMeshes
    c_x, c_y = 0.2,0.2
    degree_u = 2
    degree_p = 1

    mesh_0 = Mesh()
    with XDMFFile("mesh_0.xdmf") as infile:
        infile.read(mesh_0)    
    mesh_1 = Mesh()
    with XDMFFile("mesh_1.xdmf") as infile:
        infile.read(mesh_1)

    multimesh = MultiMesh()
    for mesh in [mesh_0, mesh_1]:
        multimesh.add(mesh)
    multimesh.build(2*degree_u)
    multimesh.auto_cover(0, Point(c_x,c_y))

    num_parts = multimesh.num_parts()
    # build function spaces
    V_el = VectorElement("CG", mesh_0.ufl_cell(), degree_u)
    V = MultiMeshFunctionSpace(multimesh, V_el)

    Q_el = FiniteElement("CG", mesh_0.ufl_cell(), degree_p)
    Q = MultiMeshFunctionSpace(multimesh, Q_el)

    tot = 0
    print("total dofs V", V.dim())
    inactive_dofs = 0
    for part in range(V.multimesh().num_parts()):
        inactive_dofs += len(V.dofmap().inactive_dofs(V.multimesh(), part))
    print("inactive dofs", inactive_dofs)
    tot += V.dim() - inactive_dofs
    print("total dofs Q", Q.dim())
    inactive_dofs = 0
    for part in range(Q.multimesh().num_parts()):
        inactive_dofs += len(Q.dofmap().inactive_dofs(V.multimesh(), part))
    print("inactive dofs", inactive_dofs)
    tot += Q.dim() - inactive_dofs
    return tot

if __name__ == "__main__":
    tot_dof = analyse_mesh()
    print(tot_dof)
